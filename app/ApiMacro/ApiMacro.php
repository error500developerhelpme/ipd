<?php
namespace App\ApiMacro;
use Symfony\Component\HttpFoundation\Response;

class ApiMacro
{
    public function apiResponse()
    {

        return function (string $message = '',object $data = null,$errors = null,string $status ='success' ,int $responseCode = Response::HTTP_OK)
        {
            return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => $data,
                'errors' => $errors,
                ],$responseCode);
        };
    }
}

