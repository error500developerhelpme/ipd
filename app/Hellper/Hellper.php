<?php


namespace App\Hellper;


use App\Models\User;
use File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Hellper
{
    /**
     * @param $data
     * @param $user_id
     * @return string
     */
    public static function QrCodeGenerator($data, $user_id, $public = 'public/')
    {
        // https://www.simplesoftware.io/#/docs/simple-qrcode/ru

        $random = Str::random(10);
        if (!is_dir(storage_path("/app/public/qr/user/".$user_id))) {
            if(!is_dir(storage_path("/app/public/qr"))) {
                File::makeDirectory(storage_path("app/public/qr"));
            }
            if(!is_dir(storage_path("/app/public/qr/user"))) {
                File::makeDirectory(storage_path("app/public/qr/user"));
            }
            File::makeDirectory(storage_path("app/public/qr/user/".$user_id));
        }
        $base64fileString = QrCode::format('png')->generate($data, storage_path('app/public/qr/user/'.$user_id.'/'.$random.'.png'), 'image/png');

        $path = '/storage/'.$public.'qr/user/'.$user_id.'/'.$random.'.png';

        return $path;
    }

    /**
     * @param $file
     * @param $path
     * @param string $option
     * @param string $replace
     * @return string
     */
    public static function image($file, $path, $option = 'public', $replace = 'storage/public')
    {
        if (is_string($file)){
            return true;
        }

        $extension = $file->getClientOriginalExtension();
        $folder = $path.time().'.'.$extension;
        Storage::disk('local')->put($folder, file_get_contents($file), $option);


        return env('APP_URL').'/'.Str::replace('public', $replace, $folder);
    }


    public static function authApi($type)
    {
        if ($type == 'admin'){
           return auth('admin-api')->user();
        }
        return auth('client-api')->user();
    }


    public static function userWithId($id)
    {
        $user = User::query()->findOrFail($id);

        $data = [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'image' => $user->path,
        ];

        return $data;
    }


}
