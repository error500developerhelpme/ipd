<?php

namespace App\Http\Controllers\Api\Admin;

use File;
use App\Models\Agreement;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\AdminService;
use App\Services\StripeService;
use App\Services\AgreementsService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\AdminRegisterRequest;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Http\Requests\CreateAgreementRequest;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public $agreementsService;

    public $adminService;

    public $userService;

    public $stripeService;

    public function __construct(
        AgreementsService $agreementsService,
        AdminService $adminService,
        UserService $userService,
        StripeService $stripeService) {

        $this->agreementsService = $agreementsService;
        $this->adminService = $adminService;
        $this->userService = $userService;
        $this->stripeService = $stripeService;
    }

    //admis-----------------

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->apiResponse('',$this->adminService->getAdminAccounts());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\AdminRegisterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRegisterRequest $request)
    {
        if (Gate::allows('create', auth('admin-api')->user()))
       {
            return response()->apiResponse('',$this->adminService->createAdmin($request));
       }else{
            return response()->apiResponse("You do not have the right to create",null,null,
            'Fail',Response::HTTP_FORBIDDEN);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->apiResponse('',$this->adminService->getAdminAccount($id));
    }

    public function showProfile()
    {
        $id = auth('admin-api')->user()->id;

        return response()->apiResponse('',$this->adminService->getAdminAccount($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\AdminRegisterRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminRegisterRequest $request, $id)
    {
       if (Gate::allows('update', auth('admin-api')->user()))
       {
            return response()->apiResponse('Account has been updated',$this->adminService->updateAdminAccount($request, $id));
       }else{
            return response()->apiResponse("You do not have the right to update",null,null,
                   'Fail',Response::HTTP_FORBIDDEN);
       }
    }

    public function updateProfile(AdminRegisterRequest $request)
    {
        $id = auth('admin-api')->user()->id;

        return response()->apiResponse('Account has been updated',$this->adminService->updateAdminAccount($request, $id));
    }

    public function updatePassword(Request $request)
    {
        $id = auth('admin-api')->user()->id;

        return $this->adminService->updatePassword($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if (Gate::allows('delete', auth('admin-api')->user()))
       {
         return response()->apiResponse('Account has been deleted',$this->adminService->deleteAccount($id));
       }else{
        return response()->apiResponse("You do not have the right to delete",null,null,
        'Fail',Response::HTTP_FORBIDDEN);
       }
    }

    //Agreements-----------

    public function adminGetAgreements($type = 'all')
    {
        return response()->apiResponse('',$this->agreementsService->adminGetAgreements(request()->get('type',$type),10));
    }

    public function detailsAgreementById($id)
    {
        return response()->apiResponse('',$this->agreementsService->detailsAgreementById($id));
    }

    public function deleteAgreementById($id)
    {
        $this->agreementsService->deleteAgreementById($id);

        return response()->apiResponse('Agreement has been deleted',null);
    }

    //Customers-----------

    public function getCustomers()
    {
        return $this->userService->getUsers('customer');
    }

    public function getCustomer($id)
    {
        return $this->userService->getUser($id);
    }

    public function deleteCustomer($id)
    {
        $this->userService->deleteUser($id);

        return response()->apiResponse('Customer has been deleted',null);
    }



    //Proprietor
    public function getProprietors()
    {
        return $this->userService->getUsers('proprietor');
    }
    public function getProprietor($id)
    {
        return $this->userService->getUser($id);
    }

    public function deleteProprietor($id)
    {
        $this->userService->deleteUser($id);

        return response()->apiResponse('Proprietor has been deleted',null);
    }

    //Proposals
    public function getProposals()
    {
        return $this->adminService->getProposals();
    }

    public function getSingleAgreementProposal($proposal_id)
    {
        return $this->adminService->getSingleAgreementProposal($proposal_id);
    }

    //show insurance,valuation and verivication lists
    /**
     * Undocumented function
     *
     * @param [type] $type = insurance,concept,certVerify
     * @return void
     */
    public function verifyAdminAgreements($type)
    {
        return $this->agreementsService->verifyAdminAgreements($type);
    }

     //plans
     public function createPlan(Request $request)
     {
         $validator = Validator::make($request->all(),[
             'amount' => ['required', 'string'],
             'name' => ['required', 'string'],
             'billing_period' => ['required', 'string'], //,'in:year,month,week,day'
             'desc' => ['required','string','max:22'],

         ]);

//         dd($validator->getMessageBag());
         if ($validator->fails()) {
             return response()->apiResponse('The given data was invalid.',
                                null,
                                $validator->errors(),
                                'Fail',
                                Response::HTTP_BAD_REQUEST);
         }

         $input = $validator->validated();


         if ($this->stripeService->createPlan($input))
         {
             return response()->apiResponse('Plan has been created',$this->stripeService->createPlan($request));
         }


         return response()->apiResponse("Plan does not created",null,null,'Fail', Response::HTTP_BAD_REQUEST);

     }

     public function deletePlan($id)
     {
        try {

            if (Gate::allows('delete', auth('admin-api')->user()))
            {
                 $this->stripeService->deletePlan($id);

                 return response()->apiResponse('Plan has been deleted',null);

            }else{

             return response()->apiResponse("You do not have the right to delete",null,
             null,'Fail',Response::HTTP_FORBIDDEN);
            }

        } catch (\Throwable $th) {

            return response()->apiResponse("No such price:",null,
            null,'Fail',Response::HTTP_NOT_FOUND);
        }


     }

     public function getPlans()
     {
        return response()->apiResponse('',$this->stripeService->getPlans());
     }

     public function updatePlan(Request $request,$id)
     {
        if (Gate::allows('update', auth('admin-api')->user()))
       {
        $validator = Validator::make($request->all(),[
            'amount' => ['required', 'string'],
            'name' => ['required', 'string'],
            'billing_period' => ['required', 'string','in:year,month,week,day'],
            'desc' => ['required','string','max:22'],

        ]);

        if ($validator->fails()) {
            return response()->apiResponse(
                               'The given data was invalid.',
                               null,
                               $validator->errors(),
                               'Fail',
                               Response::HTTP_BAD_REQUEST);
        }

        $input = $validator->validated();

        $this->stripeService->updatePlan($input,$id);

        return response()->apiResponse('Plan has been updated',null);

       }else{
            return response()->apiResponse("You do not have the right to update",null,null,
                   'Fail',Response::HTTP_FORBIDDEN);
       }

     }



}
