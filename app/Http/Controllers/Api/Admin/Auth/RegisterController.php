<?php

namespace App\Http\Controllers\Api\Admin\Auth;

use App\Hellper\Hellper;
use App\Models\Admin;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Password;
use App\Http\Requests\AdminRegisterRequest;

class RegisterController extends Controller
{
    public function register(AdminRegisterRequest $request)
    {
        if ($request->hasFile('image'))
        {
          $image = $request->file('image');
            $pathPhoto = Hellper::image($image, 'admin/profile/'.$request->user()->id.'/admin-'.$request->user()->id.'-', 'private', 'storage/admin');
//            $folder = 'admin/profile/'.$request->user()->id.'/admin-'.$request->user()->id.'.jpg';
//            $imagePath = Storage::disk('local')->put($folder,file_get_contents($image),'private');
        }else{
            $pathPhoto = '';
        }



       $user = Admin::create([
                'name' => $request['name'],
                'email' => $request['email'],
//                'image' => env('APP_URL').'/'.Str::replace('public', 'storage', $imagePath),
                'image' =>$pathPhoto,
                'password' => Hash::make($request['password']),
        ]);
        $user->roles()->create(['name' => $request['role']]);
        $token = $user->createToken('projectToken',['admin'])->accessToken;

        return response()->json(['_token'=>$token]);
    }
}
