<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Insurance;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\InsuranceResource;
use App\Models\Agreement;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class InsuranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InsuranceResource::collection(Insurance::has('agreement')->orderBy('id','desc')->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'company_name' => ['required', 'string', 'max:255'],
            'type_of_policy' => ['required', 'string', 'max:255'],
            'value_of_policy' => ['required', 'integer'],
            'policy_date' => ['required', 'date'],
            'expiry_date' => ['required', 'date'],
            'policy_image' => ['required','file','image'],
            'agreement_id' => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return response()->apiResponse(
                               'The given data was invalid.',
                               null,
                               $validator->errors(),
                               'Fail',
                               Response::HTTP_BAD_REQUEST);
        }
        $input = $validator->validated();

          $image = $request->file('policy_image');
          $folder = 'public/admin/agreement/insurances/insurance-'.time().'.jpg';
          Storage::disk('local')->put($folder,file_get_contents($image),'private');

        $agreement = Agreement::find($request['agreement_id']);

        $insurance = $agreement->insurance()->create([
            'company_name' => $input['company_name'],
            'type_of_policy' => $input['type_of_policy'],
            'value_of_policy' => $input['value_of_policy'],
            'policy_date' => $input['policy_date'],
            'expiry_date' => $input['expiry_date'],
            'policy_image' => env('APP_URL').'/'.Str::replace('public', 'storage',$folder),
        ]);

        return new InsuranceResource($insurance);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function show(Insurance $insurance)
    {
       return new InsuranceResource($insurance);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Insurance $insurance)
    {
        $validator = Validator::make($request->all(),[
            'company_name' => ['required', 'string', 'max:255'],
            'type_of_policy' => ['required', 'string', 'max:255'],
            'value_of_policy' => ['required', 'integer'],
            'policy_date' => ['required', 'date'],
            'expiry_date' => ['required', 'date'],
            'policy_image' => ['required','file','image']
        ]);

        if ($validator->fails()) {
            return response()->apiResponse(
                               'The given data was invalid.',
                               null,
                               $validator->errors(),
                               'Fail',
                               Response::HTTP_BAD_REQUEST);
        }
        $input = $validator->validated();

          $image = $request->file('policy_image');
          $folder = 'public/admin/agreement/insurances/insurance-'.time().'.jpg';
          Storage::disk('local')->put($folder,file_get_contents($image),'private');



          $insurance->update([
            'company_name' => $input['company_name'],
            'type_of_policy' => $input['type_of_policy'],
            'value_of_policy' => $input['value_of_policy'],
            'policy_date' => $input['policy_date'],
            'expiry_date' => $input['expiry_date'],
            'policy_image' => env('APP_URL').'/'.Str::replace('public', 'storage',$folder),
        ]);

        return response()->apiResponse('Insurance has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Insurance  $insurance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Insurance $insurance)
    {
        $insurance->delete();
        return response()->apiResponse('Insurance has been deleted');
    }
}
