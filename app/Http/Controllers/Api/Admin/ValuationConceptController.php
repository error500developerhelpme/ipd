<?php

namespace App\Http\Controllers\Api\Admin;

use App\Models\Concept;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ValuationConceptResource;
use App\Models\Agreement;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ValuationConceptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ValuationConceptResource::collection(Concept::has('agreement')->orderBy('id','desc')->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'valuer_name' => ['required', 'string', 'max:255'],
            'value_ascertained' => ['required','boolean'],
            'currency' => ['required', 'in:usd,kny,eu'],
            'value' => ['required','integer'],
            'comment' => ['required', 'string'],
            'image' => ['required','file','image'],
            'agreement_id' => ['required', 'integer'],
        ]);

        if ($validator->fails()) {
            return response()->apiResponse(
                               'The given data was invalid.',
                               null,
                               $validator->errors(),
                               'Fail',
                               Response::HTTP_BAD_REQUEST);
        }
        $input = $validator->validated();

          $image = $request->file('image');
          $folder = 'public/admin/agreement/valuations/valuation-'.time().'.jpg';
          Storage::disk('local')->put($folder,file_get_contents($image),'private');

        $agreement = Agreement::find($request['agreement_id']);

        $valuation = $agreement->concept()->create([

            'valuer_name' => $input['valuer_name'],
            'value_ascertained' => $input['value_ascertained'],
            'currency' => $input['currency'],
            'value' => $input['value'],
            'comment' => $input['comment'],
            'image' => env('APP_URL').'/'.Str::replace('public', 'storage',$folder),
        ]);

        return new ValuationConceptResource($valuation);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Concept  $valuation
     * @return \Illuminate\Http\Response
     */
    public function show(Concept $valuation)
    {
       return new ValuationConceptResource($valuation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Concept  $valuation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Concept $valuation)
    {
        $validator = Validator::make($request->all(),[
            'valuer_name' => ['required', 'string', 'max:255'],
            'value_ascertained' => ['required','boolean'],
            'currency' => ['required', 'in:usd,kny,eu'],
            'value' => ['required','integer'],
            'comment' => ['required', 'string'],
            'image' => ['required','file','image'],
        ]);

        if ($validator->fails()) {
            return response()->apiResponse(
                               'The given data was invalid.',
                               null,
                               $validator->errors(),
                               'Fail',
                               Response::HTTP_BAD_REQUEST);
        }
        $input = $validator->validated();

          $image = $request->file('image');
          $folder = 'public/admin/agreement/valuations/valuation-'.time().'.jpg';
          Storage::disk('local')->put($folder,file_get_contents($image),'private');

          $valuation->update([
            'valuer_name' => $input['valuer_name'],
            'value_ascertained' => $input['value_ascertained'],
            'currency' => $input['currency'],
            'value' => $input['value'],
            'comment' => $input['comment'],
            'image' => env('APP_URL').'/'.Str::replace('public', 'storage',$folder),
        ]);

        return response()->apiResponse('Valuation has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Concept  $valuation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Concept $valuation)
    {
        $valuation->delete();
        return response()->apiResponse('Valuation has been deleted');
    }
}
