<?php

namespace App\Http\Controllers\Api\Admin;

use App\Hellper\Hellper;
use App\Http\Resources\Proposal\GetProposalResources;
use App\Models\Agreement;
use App\Models\NewProposal;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Verification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\VerificationResource;
use Symfony\Component\HttpFoundation\Response;
use App\Services\AgreementsService;

class VerificationController extends Controller
{
    public $agreementsService;

    public function __construct(AgreementsService $agreementsService) {

        $this->agreementsService = $agreementsService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $proposal = NewProposal::query()->with('agreement');
        return response()->json(GetProposalResources::collection($proposal->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $verify = Verification::query()->create([
            'user_id' => $request->user_id,
            'proposal_id' => $request->proposal_id,
            'ip_id' => isset($request->ip_id) ? $request->ip_id : null,
            'type' => $request->type,
            'verify' => isset($request->ip_id) ? 'ip' : 'agreement',
            'data' => null,
            'image' => null,
        ]);
        DB::commit();
        return response()->json('successfully verified [create]', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return VerificationResource
     */
    public function show(Verification $verification)
    {
        // return new VerificationResource($verification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Verification $verification
     * @return JsonResponse
     */
    public function update(Request $request, Verification $verification)
    {
        DB::beginTransaction();
        $verify = Verification::query()->create([
            'user_id' => $request->user_id,
            'proposal_id' => $request->proposal_id,
            'ip_id' => isset($request->ip_id) ? $request->ip_id : null,
            'type' => $request->type,
            'verify' => isset($request->ip_id) ? 'ip' : 'agreement',
            'data' => null,
            'image' => null,
        ]);
        DB::commit();
        return response()->json('successfully verified [create]', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Verification $verification)
    {
        // $verification->delete();
        // return response()->apiResponse('Verification has been deleted');
    }
}
