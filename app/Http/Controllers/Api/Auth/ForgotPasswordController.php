<?php
namespace App\Http\Controllers\Api\Auth;

use App\Mail\AccessCodeMail;
use App\Mail\AccessMail;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\Rules\Password as RulesPassword;

class ForgotPasswordController extends Controller
{
    public function forgotPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);


        $code = Str::random(20);
        $user = User::query()->where('email', $request->email)->first();
        $user->reset_password_code = $code;
        $user->save();



        $data = [
            'code' => $code
        ];

        $model = new AccessMail($data);
        Mail::to($request->email)->send($model);

        return \response()->json('mail send', 200);

//        $status = Password::broker('users')->sendResetLink(
//            $request->only('email')
//        );
//
//        if ($status == Password::RESET_LINK_SENT) {
//            return [
//                'status' => __($status)
//            ];
//        }
//
//        throw ValidationException::withMessages([
//            'email' => [trans($status)],
//        ]);


    }

    public function reset(Request $request)
    {
        $request->validate([
            'code' => 'required',
//            'email' => 'required|email',
            'password' => 'required',
        ]);


        $user = User::query()->where('reset_password_code', $request->code)->first();

        if (isset($user->id)){
            $user->password = Hash::make($request->password);
            $user->reset_password_code = null;
            $user->save();
        }else{
            return \response()->json('oops', 400);
        }

        return \response()->json('Successfully changed password', 200);



//        $status = Password::broker('users')->reset(
//            $request->only('email', 'password', 'password_confirmation', 'token'),
//            function ($user) use ($request) {
//                $user->forceFill([
//                    'password' => Hash::make($request->password),
//                    'remember_token' => Str::random(60),
//                ])->save();
//
//                $user->tokens()->delete();
//
//                event(new PasswordReset($user));
//            }
//        );
//
//        if ($status == Password::PASSWORD_RESET) {
//            return response([
//                'message'=> 'Password reset successfully'
//            ]);
//        }
//
//        return response([
//            'message'=> __($status)
//        ], Response::HTTP_INTERNAL_SERVER_ERROR);

    }

    public function resetPasswordSettings(Request $request)
    {
        if (Hash::check($request->old_password, $request->user()->password)){
            $request->user()->update([
                'password' => Hash::make($request->password)
            ]);
        }else{
            return \response()->json('Oops wrong password', 400);
        }
        return \response()->json('Successfully changed password', 200);
    }

    public function resetComplete()
    {
        return response()->json([
            'email' => request()->email,
            'token' => request()->token,
        ],Response::HTTP_OK);
    }




}
