<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;


class LoginController extends Controller
{

    public function login(LoginRequest $request)
    {
        $credentials = [
            'email' => $request['email'],
            'password' => $request['password'],
        ];

        if (!Auth::guard('client')->attempt($credentials,$request['remember']))
        {
            return response()->json(['message' => 'Invalid Credentials'],Response::HTTP_UNAUTHORIZED);
        }

        $token = Auth::guard('client')->user()->createToken('projectToken',['client'])->accessToken;

        return response()->json([
            '_token'=>$token,
            'user' => [
                'id' => Auth::guard('client')->user()->id,
                'name'=>Auth::guard('client')->user()->name,
                'type'=>Auth::guard('client')->user()->type,
                ]
            ],Response::HTTP_OK);
    }

    public function logout(Request $request)
    {

        $request->user()->tokens()->delete();

        return response()->json(['message' => 'Logged out',Response::HTTP_OK]);

    }
}
