<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request)
    {
       $user = User::query()->create([
                'name' => $request['name'],
                'type' => isset($request['type']) ? $request['type'] : 'user',
                'email' => $request['email'],
                'business_name' => isset($request['business_name']) ? $request['business_name'] : ' ',
                'phone_number' => $request['phone_number'],
                'password' => Hash::make($request['password']),
        ]);

        $token = $user->createToken('projectToken',['client'])->accessToken;

        return response()->json(['_token'=>$token]);
    }
}
