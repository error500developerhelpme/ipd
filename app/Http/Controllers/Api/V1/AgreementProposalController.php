<?php

namespace App\Http\Controllers\Api\V1;

use App\Hellper\Hellper;
use App\Http\Controllers\Controller;
use App\Models\AgreementProposal;
use Illuminate\Http\Request;

class AgreementProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $file = Hellper::image($request->file, 'public/agreement/agreement-');
        $agreement_proposals = AgreementProposal::query()->create([
            'proposal_id' => $request->proposal_id,
            'agreement' => json_encode($request->agreement),
            'file' => $file,
            'status' => 'pending',
        ]);

        return response()->json('successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param AgreementProposal $agreementProposal
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, AgreementProposal $agreementProposal)
    {
        $file = Hellper::image($request->file, 'public/agreement/agreement-');
        $agreement_proposals = $agreementProposal->query()->update([
            'agreement' => json_encode($request->agreement),
            'file' => $file,
            'status' => 'pending',
        ]);

        return response()->json('successfully', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
