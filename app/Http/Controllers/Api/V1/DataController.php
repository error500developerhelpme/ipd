<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use App\Models\Category;
use App\Models\Agreement;
use Illuminate\Http\Request;
use App\Services\DataService;
use App\Services\AgreementsService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DataController extends Controller
{
    public $dataservice;
    public $agreementsServic;

    public function __construct(
        DataService $dataservice,
        AgreementsService $agreementsServic
        ) {
        $this->dataservice = $dataservice;
        $this->agreementsServic = $agreementsServic;
    }

    public function index()
    {
       return $this->dataservice->getMenuNavBar();
    }

    public function getCategories()
    {
        return response()->apiResponse('',Category::get(['id','name']));
    }

    public function getHomePageData()
    {
       return response()->apiResponse('',$this->dataservice->getHomePageData());
    }

    public function agreements(Request $request,$category_id = 'all')
    {
        return $this->agreementsServic->getPublicAgreements($category_id,(int)$request->get('count',12));
    }

    public function detailsAgreementById($id)
    {
       return $this->agreementsServic->detailsAgreementById($id);
    }

    public function bannerCreate()
    {
        return response()->apiResponse('',$this->dataservice->createBanner());
    }

    public function search(Request $request)
    {
        $search = Agreement::query()
            ->where('publish', 'public')
            ->whereIn('agreement_status', ['registered', 'pending'])
            ->where('certificate_name',  'like' , '%'. $request->search . '%')
            ->orWhere('proprietor_name', 'like' , '%'. $request->search . '%')
            ->orWhere('certificate_description', 'like' , '%'. $request->search . '%')
            ->get();
        return response()->apiResponse('',$search);
    }


}
