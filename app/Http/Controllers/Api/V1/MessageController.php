<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use App\Http\Resources\Message\GetMessageResource;
use App\Http\Resources\Message\GetRoomResource;
use App\Http\Resources\User\SearchUserResource;
use App\Models\ChatMessage;
use App\Models\Message;
use App\Models\RoomMessage;
use App\Models\User;
use App\Hellper\Hellper;
use App\Hellper\PusherHellper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;

class MessageController extends Controller
{

    /**
     * @param $room_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessages($room_id)
    {
        $messages = ChatMessage::query()->where('room_id', $room_id)->get();
        $auth = Hellper::authApi('client');
        foreach($messages as $message){
            if($auth->id == $message->to_id){
                $message->update([
                    'read' => 1
                ]);
            }
        }
        return response()->json(GetMessageResource::collection($messages), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendMessage(Request $request)
    {

        DB::beginTransaction();

        $get = RoomMessage::query()->findOrFail($request->room_id);
        $from_id = Hellper::authApi('client')->id;
//        $from_id = 2;


        $chanel = 'message-chanel-'.$get->from_id.'-'.$get->to_id;
        $event = 'message-event-'.$get->from_id.'-'.$get->to_id;


        PusherHellper::pusher($chanel, $event, ['message' => $request->message]);
        // dd($chanel, $event);
            $message = new ChatMessage();
            $message->room_id = $request->room_id;
            $message->from_id = $from_id; // from
            $message->to_id = $get->to_id == $from_id ? $get->from_id : $get->to_id;  //to
            $message->message = $request->message;
            $message->read = 0;
            $message->save();

        DB::commit();

        return response()->json(new GetMessageResource($message), 200);
    }


     public function getUser($search)
     {
         $user = User::query()->where('name', 'like' , '%'. $search . '%')->get();

         return response()->json( SearchUserResource::collection($user), 200);
     }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRooms()
    {

        $user_id = Hellper::authApi('client')->id;
        $get = RoomMessage::query()->where('from_id', $user_id)->orWhere('to_id', $user_id)->get();
        return response()->json(GetRoomResource::collection($get), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRoom(Request $request)
    {
        DB::beginTransaction();
                $from = Hellper::authApi('client');

                $roomValidate = RoomMessage::query()->where('from_id', $from->id)->where('to_id', $request->to_id)->get()->toArray();
                if(count($roomValidate)){
                    return response()->json($roomValidate, 200);
                }

                $room = RoomMessage::query()->create([
                    'name' => 'message-chanel-'.$from->id.'-'.$request->to_id,
                    'from_id' => $from->id,
                    'to_id' => $request->to_id,
                ]);
        DB::commit();

        return response()->json(new GetRoomResource($room), 200);
    }

    /**
     * @param $room_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteRoom($room_id)
    {
        $roomValidate = RoomMessage::query()->findOrFail($room_id)->delete();

        return response()->json('deleted room', 200);
    }




}
