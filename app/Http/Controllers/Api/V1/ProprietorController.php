<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\UpdateAgreementRequest;
use App\Http\Resources\Proposal\GetProposalResources;
use App\Models\NewProposal;
use Illuminate\Http\Request;
use App\Services\StripeService;
use App\Services\ProposalService;
use App\Services\AgreementsService;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\ProposalRequest;
use App\Http\Requests\CreateAgreementRequest;
use App\Http\Resources\SubscriptionResource;
use App\Services\MessageService;
use Symfony\Component\HttpFoundation\Response;

class ProprietorController extends Controller
{
    public $agreementsService;

    public $proposalService;

    public $stripeService;

    public $userService;

    public $messageService;

    public function __construct(
        AgreementsService $agreementsService,
        ProposalService $proposalService,
        StripeService $stripeService,
        UserService $userService,
        MessageService $messageService) {

        $this->agreementsService = $agreementsService;
        $this->proposalService = $proposalService;
        $this->stripeService = $stripeService;
        $this->userService = $userService;
        $this->messageService = $messageService;
    }
    //user profile
    public function getUserProfile()
    {
        return $this->userService->getUser(auth('client-api')->user()->id);
    }

    public function updateProfile(Request $request)
    {
        return response()->apiResponse('Profile has been updated',$this->userService->updateProfile($request));
    }

    //payment methods crud
    public function createPaymentMethod(PaymentRequest $request)
    {
        if ($this->stripeService->createPaymentMethod($request))
        {
            return response()->apiResponse('Payment method has been created',$this->stripeService->createPaymentMethod($request));
        }
        return response()->apiResponse("Payment method does not created",null,'Fail', Response::HTTP_BAD_REQUEST);
    }

    public function getPaymentMethods()
    {
        if (!$this->stripeService->getPaymentMethods())
        {
            return response()->apiResponse("You do not have card",null);
        }

        return response()->apiResponse("",$this->stripeService->getPaymentMethods());
    }

    public function setDefaultMethod($id)
    {
        if ($this->stripeService->setDefaultMethod($id))
        {
            return response()->apiResponse('Payment method has been updated',null);
        }

        return response()->apiResponse("Don't find payment method",null,null,'Fail', Response::HTTP_BAD_REQUEST);
    }

    public function singleCharge(PaymentRequest $request)
    {
        $proposal = $this->proposalService->getUserCustomerSingleProposal($request['proposal_id']);

        if (isset($proposal) && $proposal->status == 'accepted')
        {
            $invoice = $this->stripeService->singleCharge($request);
             $path = env('APP_URL').'/api/client/invoice/ipd/'.$invoice->id;

             return response()->apiResponse("Payment has been successfully",(object)['path' => $path]);

        }else{

           return response()->apiResponse("Don't find Proposal",null,null,'Fail', Response::HTTP_BAD_REQUEST);

        }

    }

    public function deletePaymentMethod($id)
    {
        if ($this->stripeService->deletePaymentMethod($id))
        {
            return response()->apiResponse('Payment method has been deleted',null);
        }

        return response()->apiResponse("Don't find payment method",null,null,'Fail', Response::HTTP_BAD_REQUEST);
    }

    public function planIndex()
    {
        return $this->stripeService->getPlans();
    }

    /*public function planShow(Plan $plan,Request $request)
    {
        return $this->stripeService->show($plan,$request);
    }*/

    //subscription
    public function createSubscription(Request $request)
    {
        $this->stripeService->createSubscription($request);

        return response()->apiResponse('Subscriber has been created',null);
    }

    public function getSubscription()
    {
        return response()->apiResponse('',SubscriptionResource::collection($this->stripeService->getSubscription()));
    }

    public function statusSubscription(Request $request)
    {
        $sub = $this->stripeService->statusSubscription($request);

        if (!is_null($sub->ends_at)) {

            return response()->apiResponse('Subscription has been cancelled',$sub->ends_at);

        }else{

            return response()->apiResponse('Subscription has been resumed',$sub->ends_at);
        }

    }

    //Stripe Invoices

    public function invoices()
    {
        return $this->stripeService->invoices();
    }

    public function downloadInvoice($invoice_id)
    {
        return $this->stripeService->downloadInvoice($invoice_id);
    }

    //IPD invoices

    public function ipdInvoices()
    {
        return response()->apiResponse('',$this->userService->getUserIpdInvoices());
    }

    public function downloadIpdInvoice($invoice_id)
    {
        return $this->userService->downloadIpdInvoice($invoice_id);
    }

    //Proposals

    public function getAgreementProposals($type)
    {
        return response()->apiResponse('',$this->proposalService->getAgreementProposals($type));
    }

    public function getSendProposals()
    {
        return response()->apiResponse('',$this->proposalService->getSendProposals());
    }

    public function getUserCustomerSingleProposal($id)
    {
        if ($this->proposalService->getUserCustomerSingleProposal($id))
        {
            return response()->apiResponse('',$this->proposalService->getUserCustomerSingleProposal($id));

        }else{

            return response()->apiResponse('Model Not Found',null,null,'Fail', Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Proposal
     */

    public function getProposal()
    {
        return $this->messageService->getProposal();
    }


    public function createProposal(ProposalRequest $request)
    {
        return new GetProposalResources($this->proposalService->createProposal($request));
    }

    public function getSingleProposalById($id)
    {
        return response()->apiResponse('',$this->proposalService->getSingleProposalById($id));        # code...
    }

    public function updateProposal(ProposalRequest $request,$id)
    {
        $this->proposalService->updateProposal($request,$id);

        return response()->apiResponse('Proposal has been updated', null);
    }

    public function changeStatusProposal(Request $request)
    {
        $this->proposalService->changeStatusProposal($request->all());
        return response()->json('status changed', 200);        # code...
    }

    public function deleteProposal($id)
    {
        NewProposal::query()->findOrFail($id)->delete();
        return response()->apiResponse('Proposal has been deleted', null);
    }



    /**
     * [END] Proposal
     */


    //Message
    public function sendMessage(Request $request)
    {
        return response()->apiResponse('', $this->messageService->sendMessage($request));
    }


    public function updateProposalMassage(Request $request)
    {
        $this->messageService->updateProposalMassage($request);

        return response()->apiResponse('Messages has been updated',null);
    }

    //user agreements
    public function createAgreement(CreateAgreementRequest $request)
    {
        return $this->agreementsService->createAgreement($request);
    }

    //user agreements
    public function updateAgreement(UpdateAgreementRequest $request, $id)
    {
        return $this->agreementsService->updateAgreement($request, $id);
    }

    public function notificationAgreement()
    {
        return $this->agreementsService->notificationAgreement();
    }

    public function allAgreements()
    {
        return $this->agreementsService->allAgreements(6);
    }

    public function showAgreements()
    {
        return $this->agreementsService->showAgreements();
    }

    public function proprietorAgreements($type)
    {
        return $this->agreementsService->getProprietorAgreements($type,6);
    }

    public function getAgreemenById($id)
    {
       return response()->apiResponse('',$this->agreementsService->getAgreemenById($id));
    }


}
