<?php

namespace App\Http\Controllers\Api\V1;

use App\Hellper\Hellper;
use App\Http\Controllers\Controller;
use App\Http\Resources\Proposal\GetProposalResources;
use App\Http\Resources\ProposalResource;
use App\Models\NewProposal;
use App\Models\Proposal;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    /**
     * @param $type
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getAgreementRequest($type)
    {
        $agreementProposals = NewProposal::query()->whereHas('agreement',function($query){
            $query->publicAgreements('all')
                ->whereIn('agreement_status', ['registered', 'pending']);
//                ->where('user_id',request()->user('client-api')->id);
        })
            ->where('status',$type)
            ->where('user_id', Hellper::authApi('user')->id)
            ->orderBy('id','desc')
            ->get();

        return GetProposalResources::collection($agreementProposals);
    }
}
