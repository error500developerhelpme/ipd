<?php

namespace App\Http\Controllers\Api\V1;

use App\Hellper\Hellper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Token;

class StripeController extends Controller
{
    /**
     * StripeController constructor.
     */
    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    /**
     * @param Request $request
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function CreateCard(Request $request)
    {

        $userAuth = Hellper::authApi('client');
        // Set your Stripe API secret key


        $customers = Customer::all()->toArray()['data'];

        $checkCustomer = true;
        foreach ($customers as $item){
            if (($item['email'] == $userAuth->email) && ($item['id'] == $userAuth->stripe_customer_id)){
                $checkCustomer = false;
            }
        }



        // Create a Stripe customer
        if ($checkCustomer){
            $customer = Customer::create([
                'email' => $userAuth->email,
            ]);
            //save DB
            $userAuth->stripe_id = $customer->id;
            $userAuth->save();
        }else{
            $customer = Customer::retrieve($userAuth->stripe_customer_id);
        }
//        dd($customer->toArray());

        // Create a Stripe token for the card
        $token = Token::create([
            'card' => [
                'number' => $request->input('number'),
                'exp_month' => $request->input('exp_month'),
                'exp_year' => $request->input('exp_year'),
                'cvc' => $request->input('cvc'),
            ],
        ]);


        // Attach the card to the customer
        $customer->sources->create(['source' => $token->id]);

//        $x = Customer::createSource(
//            $userAuth->stripe_customer_id,
//            ['source' => 'tok_amex']
//        );


        dd($token, $customer, __METHOD__);

    }
}
