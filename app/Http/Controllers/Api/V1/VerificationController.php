<?php

namespace App\Http\Controllers\Api\V1;

use App\Hellper\Hellper;
use App\Http\Controllers\Controller;
use App\Http\Resources\Proposal\GetProposalResources;
use App\Http\Resources\Verification\GetVerificationResource;
use App\Models\Agreement;
use App\Models\NewProposal;
use App\Models\Verification;
use App\Services\AgreementsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VerificationController extends Controller
{
    public $agreementsService;

    public function __construct(AgreementsService $agreementsService) {

        $this->agreementsService = $agreementsService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $auth = Hellper::authApi('client');
        $validate = Verification::query()->where('user_id', $auth->id)->whereNull(['data', 'image'])->with(['ip', 'proposal' => function($q) use($auth){
            if ($auth->type != 'verification'){
                $q->where('status_verify', 1);
            }
        }]);
        return response()->json(GetVerificationResource::collection($validate->get()), 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
            $auth = Hellper::authApi('client');
            $file = Hellper::image($request->file, 'public/verify/'.$auth->id.'/verify-'.$auth->id.'-');

            $verify = Verification::query()->create(
                [
                    'user_id' => $auth->id,
                    'proposal_id' => isset($request->proposal_id) ? $request->proposal_id : null,
                    'ip_id' => isset($request->ip_id) ? $request->ip_id : null,
                    'type' => $auth->type,
                    'data' => json_encode($request->data),
                    'image' => $file,
                ]
            );

            if ($auth->type == 'verification'){
                $proposal = NewProposal::query()->findOrFail($request->proposal_id)->update([
                    'status_verify' => 1
                ]);
            }

        DB::commit();
        return response()->json('successfully verified [create]', 200);

    }

    /**
     * @param NewProposal $proposal
     * @return JsonResponse
     */
    public function show(NewProposal $proposal)
    {
        //
    }

    /**
     * @param Request $request
     * @param Verification $verification
     * @return JsonResponse
     */
    public function update(Request $request, Verification $verification)
    {
        DB::beginTransaction();
        $auth = Hellper::authApi('client');

        $data = [
            'user_id' => $auth->id,
            'data' => json_encode($request->data),
        ];

        if (isset($request->file)){
            $file = Hellper::image($request->file, 'public/verify/'.$auth->id.'/verify-'.$auth->id.'-');
            $data = array_merge($data, ['image' => $file,]);
        }

        $verify = $verification->update($data);

        if ($auth->type == 'verification'){
            $proposal = $verification->load('proposal')->proposal;
            $proposal->update([
                'status_verify' => 1
            ]);
        }

        DB::commit();
        return response()->json('successfully verified [update]', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Verification $verification
     * @return JsonResponse
     */
    public function destroy(Verification $verification)
    {
        DB::beginTransaction();
            $verify = $verification->load('proposal', 'ip');
            $proposal = $verify->proposal;
            $proposal->update([
                'status' => 'rejected',
                'status_verify' => 0
            ]);

            if ($verification->verify == 'ip'){
                $agreement = $verify->ip;
                $agreement->update([
                    'agreement_status' => 'rejected'
                ]);
            }
        DB::commit();
        return response()->json('Verification has been rejected', 200);
    }


    public function verifyRejectAgreement(Verification $verification)
    {
        DB::beginTransaction();
            $verify = $verification->load(['proposal' => function($q){
                $q->with('agreementProposals');
            }]);

            $agreementProposals = $verify->proposal->agreementProposals;
            $agreementProposals->update([
                'status' => 'rejected'
            ]);
        DB::commit();
        return response()->json('Agreement has been rejected', 200);
    }


}
