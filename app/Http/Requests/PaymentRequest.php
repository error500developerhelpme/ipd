<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string'],
            'number' => ['required','string'],
            'exp_month' => ['required','integer','date_format:m'],
            'exp_year' => ['required','integer','date_format:Y'],
            'cvc' => ['required','integer'],
        ];
    }
}
