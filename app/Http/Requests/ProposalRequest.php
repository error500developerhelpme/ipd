<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProposalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "agreement_id" => 'required',
            "type" => 'required',
            "certificate" => 'required',


//            "agreement_id" => ['required', 'integer'],
//            "status" => ['nullable'],
//            "acceptance" => ['required','string'],
//            "breach_of_terms" => ['required','string'],
//            "capacity" => ['required','string'],
//            "confidentiality" => ['required','integer'],
//            "consideration" => ['required','string'],
//            "due_diligence" => ['required','string'],
//            "duration" => ['required','string'],
//            "intention" => ['required','string'],
//            "indemnity" => ['required','integer'],
//            "injury" => ['required','integer'],
//            "negotiation" => ['required','string'],
//            "non_compete" => ['required','string'],
//            "notices" => ['required','string'],
//            "offer" => ['required','string'],
//            "price_cost" => ['required','integer'],
//            "terms_conditions" => ['required','integer'],
//            "termination" => ['required','string'],
//            "valuation_type" => ['required','string','in:value,insure,verify_certificate']
        ];
    }
}
