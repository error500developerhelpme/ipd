<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAgreementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=> 'required',
            'publish'=> 'required',
            'type'=> 'required',
            'photo' => 'nullable',
            'certificate_name' => 'required',
            'certificate_number' => 'required',
            'proprietor_name' => 'required',
            'name_of_concept' => 'required',
            'date_of_issue' => 'required',
            'country_issued'=> 'required',
            'certificate_description' => 'required',

           /* 'category_id'=> 'required|integer',
            'type'=> 'required',
            'photo' => 'required|file|image|max:15000',
            'certificate_name' => 'required|string|max:1000',
            'certificate_number' => 'required|integer',
            'proprietor_name' => 'required|string|max:1000',
            'name_of_concept' => 'required|string|max:1000',
            'date_of_issue' => 'required|string|date_format:Y/m/d',
            'country_issued'=> 'required|string|max:1000',
            'certificate_description' => 'required|string|max:70',*/
        ];
    }

}
