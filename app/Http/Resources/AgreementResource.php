<?php

namespace App\Http\Resources;

use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Resources\Json\JsonResource;

class AgreementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if ($request->routeIs('show-verify-list'))
        {
            return [
                "user_id" => $this->user_id,
                "certificate_name" => $this->certificate_name,
                "certificate_description" => $this->certificate_description,
            ];
        }
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "publish" => $this->publish,
            "category_id" => $this->category_id,
            "category_name" => $this->category->name ?? '',
            "type" => $this->type,
            "photo" => $this->photo,
            "certificate_name" => $this->certificate_name,
            "certificate_number" => $this->certificate_number,
            "proprietor_name" => $this->proprietor_name,
            "name_of_concept" => $this->name_of_concept,
            "date_of_issue" => $this->date_of_issue,
            "country_issued" => $this->country_issued,
            "certificate_description" => $this->certificate_description,
            "qr_code" => $this->qr_code,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "insurance" => $this->insurance->value_of_policy ?? '',
            'concept' => $this->ipCertificateConcept->value ?? '',

        ];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(Response::HTTP_OK,'success');
    }
}
