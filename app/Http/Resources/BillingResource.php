<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'pm_id' => $this->id,
            'card_holder_name' => $this->holder_name,
            'card_number' => '****'.$this->card_number_last4,
            'card_brand' => $this->brand,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'pm_method' => $this->pm_method,
            'expires' => $this->expires,
        ];
    }
}
