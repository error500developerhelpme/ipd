<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'status' => "success",
            "data" => [
            "id" => $this->id,
            "link" =>$this->link,
            "alias" => $this->alias,
            "status" => $this->status,
            "title" => $this->title,
            "descritpion" => $this->description,
            "content" => $this->content,
            'created_at' => $this->created_at,
            ],

        ];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(Response::HTTP_OK,'success');
    }
}
