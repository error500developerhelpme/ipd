<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InsuranceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "company_name" => $this->company_name,
            "type_of_policy" => $this->type_of_policy,
            "value_of_policy" => $this->value_of_policy,
            "policy_date" => $this->policy_date,
            "expiry_date" => $this->expiry_date,
            "policy_image" => $this->policy_image,
            "created_at" => $this->created_at,
        ];
    }
}
