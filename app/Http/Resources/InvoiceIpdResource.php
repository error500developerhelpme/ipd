<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceIpdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_name" => $this->user->name,
            "date_of_issue" => $this->date_of_issue,
            "date_of_due" => $this->date_of_due,
            "category_name" => $this->category->name,
            "desc" =>$this->desc,
            "qti" => $this->qti,
            "unit_price" => $this->unit_price,
            "amount" => $this->amount,
            "created_at" => $this->created_at,
        ];
    }
}
