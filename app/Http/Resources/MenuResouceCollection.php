<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Symfony\Component\HttpFoundation\Response;

class MenuResouceCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
                'status' => 'success',
                'data' => $this->collection,
             ];
    }

    public function withResponse($request, $response)
    {
        $response->setStatusCode(Response::HTTP_OK,'success');
    }
}
