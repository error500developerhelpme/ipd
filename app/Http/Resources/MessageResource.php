<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "proposal_id" => $this->proposal_id,
            "user_id" => $this->user->id,
            "user_name" => $this->user->name,
            "message" => $this->message,
            "is_seen" => $this->is_seen,
            "created_at" => $this->created_at,
            "updated_at"=> $this->updated_at,
        ];
    }
}
