<?php

namespace App\Http\Resources\Proposal;

use App\Hellper\Hellper;
use Illuminate\Http\Resources\Json\JsonResource;

class GetProposalResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" => Hellper::userWithId($this->user_id),
            "customer_id" => Hellper::userWithId($this->customer_id),
//            "agreement_id" => $this->agreement_id,
            "type" => $this->type,
            "certificate" => json_decode($this->certificate),
            "status_verify" => $this->status_verify,
            "status" => $this->status,
            "updated_at" => $this->updated_at,
            "created_at" => $this->created_at,
            "ip" => $this->agreement,
            "agreement" => $this->agreementProposals,
        ];
    }
}
