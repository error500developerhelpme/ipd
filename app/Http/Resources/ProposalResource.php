<?php

namespace App\Http\Resources;

use App\Http\Resources\MessageResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Resources\Json\JsonResource;

class ProposalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if ($request->routeIs('agreement-proposals'))
        {
            return  [
                "proposal_id"=> $this->id,
                "agreement_id" => $this->agreement->id,
                "agreement_photo" => $this->agreement->photo,
                "certificate_name" => $this->agreement->certificate_name,
                "certificate_number" => $this->agreement->certificate_number,
            ];
        }

        if ($request->routeIs('get-proposals'))
        {
            return  [
                "proposal_id"=> $this->id,
                "agreement_id" => $this->agreement->id,
                "certificate_desc" => $this->agreement->certificate_description,
                "certificate_name" => $this->agreement->certificate_name,
                "valuation_type" => $this->valuation_type,
                "certificate_number" => $this->agreement->certificate_number,
                "crete_date" => $this->created_at,
                'customer_id' => $this->customer_id,
                'status' => $this->status,
            ];
        }

        return  [

            "id" => $this->id,
            "agreement_id" => $this->agreement_id,
            "customer_id" => $this->customer_id,
            "sender_name" => $this->customer->name ?? '',
            'messages' => MessageResource::collection($this->messages) ?? '',
            "user_id" => $this->user_id,
            "status" => $this->status,
            "acceptance" => $this->acceptance,
            "breach_of_terms" => $this->breach_of_terms,
            "capacity" => $this->capacity,
            "confidentiality" => $this->confidentiality,
            "consideration" => $this->consideration,
            "due_diligence" => $this->due_diligence,
            "duration" => $this->duration,
            "intention" => $this->intention,
            "indemnity" => $this->indemnity,
            "injury" => $this->injury,
            "negotiation" => $this->negotiation,
            "non_compete" => $this->non_compete,
            "notices" => $this->notices,
            "offer" => $this->offer,
            "price_cost" => $this->price_cost,
            "terms_conditions" => $this->terms_conditions,
            "termination" => $this->termination,
            "valuation_type" => $this->valuation_type,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
        ];
    }
}
