<?php

namespace App\Http\Resources;

use App\Models\Plan;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "stripe_id" => $this->stripe_id,
            "stripe_status" => $this->stripe_status,
            "stripe_price" => Plan::where('price_id',$this->stripe_price)->first()->price ?? '',
            "ends_at" => $this->ends_at,
            "created_at" => $this->created_at,
        ];
    }
}
