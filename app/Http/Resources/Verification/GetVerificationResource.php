<?php

namespace App\Http\Resources\Verification;

use App\Http\Resources\Proposal\GetProposalResources;
use Illuminate\Http\Resources\Json\JsonResource;

class GetVerificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable
     */
    public function toArray($request)
    {
        if (empty($this->proposal)){
            return [];
        }
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'verify_type' => $this->verify,
            'proposal' => new GetProposalResources($this->proposal),
//            'ip' => $this->ip,
            'type' => $this->type,
        ];
    }
}
