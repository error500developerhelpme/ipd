<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VerificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [

            "id" => $this->id,
            "agreement_id" => $this->agreement_id,
            "autority_name" => $this->autority_name,
            "certificate_authentic" => $this->certificate_authentic,
            "discrepancy" => $this->discrepancy,
            "image" => $this->image,
            "created_at" => $this->created_at,
        ];
    }
}
