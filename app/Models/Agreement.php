<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'publish',
        'category_id',
        'insurance_id',
        'concept_id',
        'type',
        'agreement_status',
        'photo',
        'certificate_name',
        'certificate_number',
        'proprietor_name',
        'name_of_concept',
        'date_of_issue',
        'country_issued',
        'certificate_description',
        'qr_code',
    ];

    public function proprietor()
    {
        return $this->belongsTo(User::class,'user_id','id')->where('role',1);
    }

    public function scopefindUser($query,$user_id)
    {
        return $query->where('user_id',$user_id);
    }

    public function scopePublicAgreements($query,$category_id)
    {
        if ($category_id == 'all') {
            return $query->where('publish','public');
        }else{
            return $query->where('publish','public')->where('category_id',$category_id);
        }

    }

    public function newProposals()
    {
        return $this->hasMany(NewProposal::class,'agreement_id','id');
    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class,'agreement_id','id');
    }

    public function verify()
    {
        return $this->hasMany(Verification::class,'ip_id','id');
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function insurance()
    {
        return $this->hasOne(Insurance::class,'agreement_id','id');
    }

    public function concept()
    {
        return $this->hasOne(Concept::class,'agreement_id','id');
    }

    public function certVerify()
    {
        return $this->hasOne(Verification::class,'agreement_id','id');
    }

    public function request()
    {
        return $this->hasMany(RequestOrder::class,'agreement_id','id');
    }


}
