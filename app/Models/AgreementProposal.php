<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgreementProposal extends Model
{
    use HasFactory;

    protected $table = 'agreement_proposals';

    protected $fillable = [
        'proposal_id',
        'agreement',
        'file',
        'status', // 'accepted','rejected', 'registered', 'pending'
    ];

    public function proposal()
    {
        return $this->belongsTo(NewProposal::class,'proposal_id');
    }


}
