<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    use HasFactory;

    protected $fillable = [
        'pm_id',
        'card_number_last4',
        'brand',
        'holder_name',
        'status',
        'user_id',
        'pm_method',
        'expires',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
