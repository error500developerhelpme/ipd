<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Concept extends Model
{
    use HasFactory;

    protected $fillable = [
        'valuer_name',
        'value_ascertained',
        'currency',
        'value',
        'comment',
        'image',
        'agreement_id',
    ];

    public function agreement()
    {
        return $this->belongsTo(Agreement::class,'agreement_id','id');
    }
}
