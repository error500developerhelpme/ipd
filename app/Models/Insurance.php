<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    use HasFactory;

    protected $fillable = [
            'company_name',
            'type_of_policy',
            'value_of_policy',
            'policy_date',
            'expiry_date',
            'policy_image',
    ];

    public function agreement()
    {
        return $this->belongsTo(Agreement::class,'agreement_id','id');
    }
}
