<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [

       'proposal_id',
       'user_id',
       'message',
       'is_seen',

    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function proposals()
    {
        return $this->belongsTo(Proposal::class,'proposal_id');
    }
}
