<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewProposal extends Model
{
    use HasFactory;

    protected $table = 'new_proposals';

    protected $fillable = [
        'user_id',
        'customer_id',
        'agreement_id',
        'type',  // sell, license, charge, partner
        'certificate',
        'status_verify',
        'status', // 'accepted','rejected', 'registered', 'pending'
    ];



    public function verification()
    {
        return $this->hasMany(Verification::class,'proposal_id','id');
    }

    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id');
    }

    public function agreementProposals()
    {
        return $this->hasOne(AgreementProposal::class,'proposal_id','id');
    }
}
