<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        'link',
        'alias',
        'status',
        'title',
        'description',
        'content',
    ];

    protected $casts = [
        'content' => 'array',
    ];
}
