<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Proposal extends Model
{
    use HasFactory;

    protected $table = "proposals";

    protected $fillable = [
       'agreement_id',
       'user_id',
       'customer_id',
       'status',
       'acceptance',
       'breach_of_terms',
       'capacity',
       'confidentiality',
       'consideration',
       'due_diligence',
       'duration',
       'intention',
       'indemnity',
       'injury',
       'negotiation',
       'non_compete',
       'notices',
       'offer',
       'price_cost',
       'terms_conditions',
       'termination',
       'valuation_type',
    ];

    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class,'proposal_id','id')->orderBy('id','desc');
    }

}
