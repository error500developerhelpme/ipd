<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [

      'proposal_id',
      'user_id',
      'pm_id',
      'currency',
      'amount',
      'pi_id',
      'ch_id',
      'status',
    ];
}
