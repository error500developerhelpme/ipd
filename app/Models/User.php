<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'email',
        'image',
        'business_name',
        'phone_number',
        'status',
        'password',
        'reset_password_code',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {

        $url = env('APP_URL').'api/client/reset-password?email='.$this->email.'&token=' . $token;

        $this->notify(new ResetPasswordNotification($url));
    }

    public function agreements()
    {
        return $this->hasMany(Agreement::class,'user_id','id');
    }

    public function newProposalsCustomer()
    {
        return $this->hasMany(NewProposal::class,'customer_id','id');
    }

    public function newProposalsUser()
    {
        return $this->hasMany(NewProposal::class,'user_id','id');
    }

    public function proposals()
    {
        return $this->hasMany(Proposal::class,'customer_id','id');
    }

    public function scopeCustomer($query)
    {
        return $query->has('proposals');
    }

    public function scopeProprietor($query)
    {
        return $query->has('agreements');
    }

    public function billings()
    {
        return $this->hasMany(Billing::class);
    }

    public function ipdInvoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function requestOder()
    {
        return $this->hasMany(RequestOrder::class,'user_id','id');
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }


    public function roles()
    {
        return $this->belongsToMany(Role::class,'role_user','user_id','role_id');
    }
}
