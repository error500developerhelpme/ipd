<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    use HasFactory;

    protected $fillable = [
            'user_id',
            'proposal_id',
            'ip_id',
            'verify',
            'type', //  "verification", "valuation ", "insurance"
            'data',
            'image',
    ];

    public function proposal()
    {
        return $this->belongsTo(NewProposal::class,'proposal_id','id');
    }

    public function ip()
    {
        return $this->belongsTo(Agreement::class,'ip_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
