<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(Admin $admin)
    {
        foreach($admin->roles as $role)
        {
            if ($role->name == 'Super_Admin')
            {
               return true;
            }
        }

        return false;
    }

    public function update(Admin $admin)
    {
        foreach($admin->roles as $role)
        {
            if ($role->name == 'Super_Admin')
            {
               return true;
            }
        }

        if($admin->id === auth('admin-api')->user()->id)
        {
            return true;
        }

        return false;
    }

    public function delete(Admin $admin)
    {
        foreach($admin->roles as $role)
        {
            if ($role->name == 'Super_Admin')
            {
               return true;
            }
        }

        return false;
    }
}
