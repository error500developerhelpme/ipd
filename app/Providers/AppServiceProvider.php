<?php

namespace App\Providers;

use App\ApiMacro\ApiMacro;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @throws \ReflectionException
     */
    public function boot()
    {
        ResponseFactory::mixin(new ApiMacro);
    }
}
