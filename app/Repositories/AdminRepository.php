<?php
namespace App\Repositories;

use File;
use App\Models\Admin;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Repositories\BaseRepository\BaseRepository;

class AdminRepository extends BaseRepository
{

    public function __construct(Admin $model)
	{
		$this->model = $model;
	}

    public function createAdmin($request)
    {
        if ($request->hasFile('image'))
        {
          $image = $request->file('image');
          $folder = 'public/admin/profile/admin-'.time().'.jpg';
          Storage::disk('local')->put($folder,file_get_contents($image),'private');

        }

       $user = $this->create([
                'name' => $request['name'],
                'email' => $request['email'],
                'image' => env('APP_URL').'/'.Str::replace('public', 'storage',$folder),
                'password' => Hash::make($request['password']),
        ]);

        $user->roles()->sync(['role_id' => $request['role']]);

        return $user;
    }

    public function getAdminAccounts()
    {
       return $this->get();
    }

    public function getAdminAccount($id)
    {
       return $this->getById($id);
    }

    public function updatePassword($id,$date)
    {
        return $this->updateById($id,$date);
    }

    public function updateAdminAccount($request,$id)
    {
        $path =  $this->getById($id)->image;

        $slice = Str::after($path, 'profile/');

        if ($request->hasFile('image'))
        {
          $image = $request->file('image');
          $folder = 'public/admin/profile/'.$slice;
          Storage::disk('local')->put($folder,file_get_contents($image),'private');


        }

        $data = [
            'name' => $request['name'],
            'email' => $request['email'],
            'image' => env('APP_URL').Str::replace('public', 'storage', $folder),
        ];

       return $this->updateById($id,$data);
    }

    public function deleteAccount($id)
    {
         $this->getById($id)->roles()->sync([]);
         $this->deleteById($id);

    }



}
