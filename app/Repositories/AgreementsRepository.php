<?php
namespace App\Repositories;

use App\Http\Requests\CreateAgreementRequest;
use App\Models\Agreement;
use App\Repositories\BaseRepository\BaseRepository;
use File;

class AgreementsRepository extends BaseRepository
{
    const SELL = 'sell';
    const LICENSE = 'license';
    const CHARGE = 'charge';
    const PARTNER = 'partner';

    public function __construct(Agreement $model)
	{
		$this->model = $model;
	}

    //Admin side
    public function adminGetAgreements($type,$count)
    {
        if ($type == 'all')
        {
            return $this->orderBy('id','desc')->paginate($count);
        }else {
            return $this->where('type',$type)->orderBy('id','desc')->paginate($count);
        }

    }

    public function verifyAdminAgreements($type)
    {
        return $this->has($type)->paginate(10);
    }

    public function createAgreement($data)
    {

        return $this->create($data);
    }

    public function notificationAgreement()
    {
        $this->scopes = ['findUser' =>[request()->user('client-api')->id]];

        return $this
            ->orderBy('id','desc')
            ->getByCol(['id','user_id','certificate_number','certificate_name','agreement_status']);
    }

    public function getPublicAgreements($category_id,$count)
    {
        $this->scopes = ['publicAgreements' => [$category_id]];

        if (request()->user('client-api'))
        {
            return $this->where('user_id',request()->user('client-api')->id,'!=')->paginate($count);
        }else{

            return $this->orderBy('id','desc')->paginate($count);
        }

    }


    public function getSellAgreements($count)
    {
        $this->scopes = ['findUser' =>[request()->user('client-api')->id]];

        return $this
            ->where('type',self::SELL)
            ->orderBy('id','desc')->paginate($count);
    }
    public function getLicenseAgreements($count)
    {
        $this->scopes = ['findUser' =>[request()->user('client-api')->id]];

        return  $this
            ->where('type',self::LICENSE)
            ->orderBy('id','desc')->paginate($count);
    }
    public function getChargeAgreements($count)
    {
        $this->scopes = ['findUser' =>[request()->user('client-api')->id]];

        return  $this
            ->where('type',self::CHARGE)
            ->orderBy('id','desc')->paginate($count);
    }
    public function getPartnerAgreements($count)
    {
        $this->scopes = ['findUser' =>[request()->user('client-api')->id]];

        return  $this
            ->where('type',self::PARTNER)
            ->orderBy('id','desc')->paginate($count);
    }

    public function getAgreemenById($id)
    {
        $this->scopes = ['findUser' =>[request()->user('client-api')->id]];

        return  $this->getById($id);
    }

    public function detailsAgreementById($id)
    {
        return  $this->getById($id);
    }

    public function deleteAgreementById($id)
    {
        return $this->deleteById($id);
    }

    public function updateAgreement($id,$data)
    {
        return $this->updateById($id,$data);
    }
}
