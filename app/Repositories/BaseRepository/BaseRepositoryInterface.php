<?php
namespace App\Repositories\BaseRepository;
use Closure;

interface BaseRepositoryInterface {

	public function all();

	public function count();

	public function create(array $data);

	public function createMultiple(array $data);

	public function delete();

	public function deleteById($id);

	public function deleteMultipleById(array $ids);

	public function first();

    public function paginate($count);

	public function get();

	public function getById($id);

	public function limit($limit);

	public function orderBy($column, $value);

	public function updateById($id, array $data);

    public function update(array $data);

	public function where($column, $value, $operator = '=',$boolean = 'and');

    public function whereGroup(Closure $callback = null);

	public function whereIn($column, $value);

    public function has($relation, $operator = '>=', $count = 1);

    public function orHas($relation, $operator = '>=', $count = 1);

    public function whereHas($relation, Closure $callback = null, $operator = '>=', $count = 1);

    public function orWhereHas($relation, Closure $callback = null, $operator = '>=', $count = 1);

	public function with($relations);

}
