<?php
namespace App\Repositories;

use App\Models\Billing;
use App\Repositories\BaseRepository\BaseRepository;

class BillingRepository extends BaseRepository
{

    public function __construct(Billing $model) {
        $this->model = $model;
    }

    public function updateBillingDefault($pm_id)
    {

        $this->where('user_id',request()->user('client-api')->id)->update(['pm_method' => 0]);

        return $this->where('pm_id',$pm_id)->update(['pm_method' => 1]);
    }

    public function getBillings()
    {
        return request()->user('client-api')->billings()->get();
    }

}
