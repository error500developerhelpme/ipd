<?php
namespace App\Repositories;

use App\Models\Page;
use App\Repositories\BaseRepository\BaseRepository;

class DataRepository extends BaseRepository
{

    public function __construct(Page $model)
	{
		$this->model = $model;
	}

    public function getMenuNavBar()
    {
       return $this->where('status',true)->getByCol(['link','status','title','description']);
    }

    public function getHomePageData()
    {
        return $this->where('status',true)->whereJsonContains('content->banner', 'en')->first();
    }

    public function getAgreements($count)
    {
        return $this->where('status',true)->paginate($count);
    }

    public function createHomePageData()
    {
        $banners = $this->where('alias','home-page')->firstByCol('content');

        $arr = $banners->content['banner'];
        array_push($arr,['title1' => "111111titlnerrrrrrrrrrrrrrrrrrrr", 'image1' => "11111111linkerrrrrrrrrrrrrrrrrrrrr"]);

        $this->model->where('alias','home-page')->update(['content->banner' => $arr]);



       /* $this->create([
            'link' => '/home',
            'alias' => 'home-page',
            'status' => true,
            'title' => "some text",
            'descriptin' => 'ttttttdfgfgdgd',
            'content' =>[
                "banner" =>[
                    ['title' => "titlnerrrrrrrrrrrrrrrrrrrr",'image' => "linkerrrrrrrrrrrrrrrrrrrrr"],
                ],
            ]
            ]); */
    }
}
