<?php
namespace App\Repositories;

use App\Models\Invoice;
use App\Repositories\BaseRepository\BaseRepository;

class IpdInvoiceRepository extends BaseRepository
{

    public function __construct(Invoice $model) {
        $this->model = $model;
    }

    public function getUserIpdInvoices($user_id)
    {
       return $this->where('user_id',$user_id)->orderBy('id','desc')->getByCol(['id','date_of_issue','date_of_due']);
    }

    public function downloadIpdInvoice($invoice_id)
    {
        return $this->where('id',$invoice_id)->first();
    }

    public function createIpdInvoice($data)
    {
        return $this->create($data);
    }

}
