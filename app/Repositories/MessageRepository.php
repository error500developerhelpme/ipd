<?php
namespace App\Repositories;

use App\Http\Resources\MessageResource;
use App\Models\Message;
use App\Repositories\BaseRepository\BaseRepository;


class MessageRepository extends BaseRepository
{

    public function __construct(Message $model)
	{
		$this->model = $model;
	}

    public function send($request)
    {
       return $this->create([
            'proposal_id' => $request['proposal_id'],
            'user_id' => $request->user('client-api')->id,
            'message' => $request['message'],
        ]);
    }

    public function getProposal()
    {

        $messages = $this->model->with('proposals')
                ->whereHas('proposals',function($query){
                    $query
                    ->where('customer_id',request()->user('client-api')->id)
                    ->orwhere(function ($query){
                        $query->whereHas('agreement',function($query){
                            $query->where('user_id',request()->user('client-api')->id);
                        });
                    });
                })
               // ->where('is_seen',false)
                ->where('user_id','!=',request()->user('client-api')->id)
                ->groupBy(['proposal_id','user_id','is_seen'])
                ->get(['proposal_id','user_id','is_seen']);


        return $messages;
    }

    public function updateProposalMassage($request)
    {
      return $this->
                where('proposal_id',$request['proposal_id'])->
                where('user_id',$request['user_id'])->update([
                    'is_seen' => true,
                ]);
    }

}
