<?php
namespace App\Repositories;

use App\Models\Plan;
use App\Repositories\BaseRepository\BaseRepository;

class PlanRepository extends BaseRepository
{

    public function __construct(Plan $model) {
        $this->model = $model;
    }

}
