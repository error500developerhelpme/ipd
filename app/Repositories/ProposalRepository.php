<?php
namespace App\Repositories;

use App\Hellper\Hellper;
use App\Http\Requests\ProposalRequest;
use App\Models\Agreement;
use App\Models\NewProposal;
use App\Models\Proposal;
use App\Repositories\BaseRepository\BaseRepository;
use Illuminate\Support\Facades\DB;


class ProposalRepository extends BaseRepository
{
    const NEW = 'null';
    const ACCEPTED = 'accepted';
    const REJECTED = 'rejected';

    public function __construct(NewProposal $model)
	{
		$this->model = $model;
	}
    //Admin side
    public function getAdminAgreementProposals()
    {
        $agreementProposals = $this->whereHas('agreement',function($query){
            $query->publicAgreements('all')
                  ->whereIn('agreement_status',['registered', 'pending']);
        })
        ->orderBy('id','desc')
        ->paginate(10);

        return $agreementProposals;
    }

    public function getSingleAgreementProposal($proposal_id)
    {
        return $this->getById($proposal_id);
    }
    //Get incoming proposals
    public function getAgreementProposals($type)
    {
        $agreementProposals = $this->whereHas('agreement',function($query){
            $query->publicAgreements('all')
                  ->whereIn('agreement_status', ['registered', 'pending']);
//                  ->where('user_id',request()->user('client-api')->id);
        })
        ->where('status',$type)
        ->where('customer_id', Hellper::authApi('user')->id)
        ->orderBy('id','desc')
        ->get();

        return $agreementProposals;
    }

    public function getSingleProposalById($id)
    {
        $proposal = NewProposal::query()->findOrFail($id)->load('agreement');
        return $proposal;
    }

    //create proposal
    public function createProposal($request, $agreements)
    {
        DB::beginTransaction();
        if ($agreements){
            $proposal = NewProposal::query()->create([
                'user_id' => $agreements['user_id'],
                'customer_id' => Hellper::authApi('user')->id,
                'agreement_id' => $agreements['id'],
                'type' => $request['type'],
                'certificate'  => json_encode($request['certificate']),
                'status_verify' => null,
                'status' => 'pending',
            ]);
            DB::commit();
            return $proposal;
        }
        else{
            DB::rollBack();
            return response()->json('oops',400);
        }


    }

    public function changeStatusProposal($request)
    {
        DB::beginTransaction();
            $proposal = NewProposal::query()->findOrFail($request['id'])->update([
                'status' => $request['status'],
            ]);
        DB::commit();
        return $proposal;
    }



    public function updateProposal($request,$id)
    {
        if (isset($request['certificate'])){
            $data = [
                'certificate'  => json_encode($request['certificate']),
            ];
        }else{
            $data = [
                'type'  => $request['type'],
            ];
        }
        $proposal = NewProposal::query()->findOrFail($id)->update($data);
        return $proposal;
//        return $this->updateById($id,$request->all());
    }

    public function getSendProposals()
    {
        return $this
                ->where('customer_id',auth('client-api')->user()->id)
                ->orderBy('id','desc')
                ->paginate(10);
    }

    public function getUserCustomerSingleProposal($id)
    {
        try {
            return $this->model->where('id',$id)->where(function($query){
                $query
                    ->where('customer_id',auth('client-api')->user()->id)
                    ->orWhere('user_id',auth('client-api')->user()->id);
            })->first();

        } catch (\Throwable $th) {

            return false;
        }

    }

}
