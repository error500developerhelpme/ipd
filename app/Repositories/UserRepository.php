<?php
namespace App\Repositories;

use App\Hellper\Hellper;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Repositories\BaseRepository\BaseRepository;

class UserRepository extends BaseRepository
{

    public function __construct(User $model)
	{
		$this->model = $model;
	}

    public function getUsers($userRole)
    {
      $this->scopes = [$userRole =>[]];

       return $this->paginate(10);
    }

    public function getUser($id)
    {
       return $this->getById($id);
    }

    public function deleteUser($id)
    {
        return $this->deleteById($id);
    }

    public function updateProfile($request,$id)
    {

        if ($request->hasFile('image'))
        {
          $image = $request->file('image');
          $path = Hellper::image($image, 'public/user/profile/user-');
            $data = [
                'name' => $request['name'],
//                'email' => $request['email'],
                'business_name' => $request['business_name'],
                'phone_number' => $request['phone_number'],

                'image' => $path,
            ];
        } else{
            $data = [
                'name' => $request['name'],
//                'email' => $request['email'],
                'business_name' => $request['business_name'],
                'phone_number' => $request['phone_number'],
            ];
        }



       return $this->updateById($id,$data);
    }
}
