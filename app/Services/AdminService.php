<?php
namespace App\Services;
use Illuminate\Support\Facades\Hash;
use App\Repositories\AdminRepository;
use App\Services\AgreementsService;
use App\Services\ProposalService;
use Illuminate\Validation\Rules\Password;
use Symfony\Component\HttpFoundation\Response;

class AdminService
{
    public $adminRepository;

    public $agreementsService;

    public $proposalService;

    public function __construct(
        AdminRepository $adminRepository,
        AgreementsService $agreementsService,
        ProposalService $proposalService
        )

    {
        $this->adminRepository = $adminRepository;
        $this->agreementsService = $agreementsService;
        $this->proposalService = $proposalService;
    }
    //proposals
    public function getProposals()
    {
        return $this->proposalService->getAdminAgreementProposals();
    }

    public function getSingleAgreementProposal($proposal_id)
    {
        return $this->proposalService->getSingleAgreementProposal($proposal_id);
    }

    public function updatePassword($request,$id)
    {
        $request->validate([
            'old_password' => ['required'],
            'password' => ['required','confirmed',Password::min(8)],
        ]);

        if(!Hash::check($request->old_password, auth('admin-api')->user()->password)){

             return response()->apiResponse("Old Password Doesn't match!",null,
             'Fail',Response::HTTP_FORBIDDEN);
        }

        $date = [
            'password' => Hash::make($request->new_password),
        ];

        return $this->adminRepository->updatePassword($id,$date);
    }

    public function createAdmin($request)
    {
        return $this->adminRepository->createAdmin($request);
    }
    public function getAdminAccounts()
    {
       return $this->adminRepository->getAdminAccounts();
    }

    public function getAdminAccount($id)
    {
       return $this->adminRepository->getAdminAccount($id);
    }

    public function updateAdminAccount($request,$id)
    {
        return $this->adminRepository->updateAdminAccount($request,$id);
    }

    public function deleteAccount($id)
    {
        return $this->adminRepository->deleteAccount($id);
    }

}
