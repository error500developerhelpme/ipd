<?php
namespace App\Services;

use App\Hellper\Hellper;
use App\Models\Agreement;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\AgreementResource;
use App\Http\Resources\ProposalResource;
use App\Repositories\AgreementsRepository;
use App\Http\Resources\ProprietorCollection;
use Symfony\Component\HttpFoundation\Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use File;

class AgreementsService
{
    public $agreementsRepository;

    public function __construct(AgreementsRepository $agreementsRepository) {
        $this->agreementsRepository = $agreementsRepository;
    }

    //Admin side
    public function verifyAdminAgreements($type)
    {
        return  AgreementResource::collection($this->agreementsRepository->verifyAdminAgreements($type));
    }

    public function adminGetAgreements($type,$count)
    {
        return AgreementResource::collection($this->agreementsRepository->adminGetAgreements($type,$count));
    }

    public function createQrCodVerifyed($agreement)
    {
            ///QrCode
            $qRfolder= storage_path().'/app/public/images/qrCode/'.$agreement['user_id'].'/';
            $filename =time().'_'.$agreement['id'].'.png';
            $path = $qRfolder.$filename;

            File::makeDirectory($qRfolder, 0700, true, true);

            $qrCode = QrCode::format('png')->size(200)->generate(env('APP_URL').'/api/v1/details-agreement/'.$agreement['id'], $path);
            Storage::setVisibility('public/images/qrCode/'.$agreement['user_id'].'/'.$filename, 'public');

            return $this->agreementsRepository->updateAgreement($agreement['id'],[
                'qr_code' => env('APP_URL').'/storage/images/qrCode/'.$agreement['user_id'].'/'.$filename,
                'agreement_status' => 'registered' ]);
    }

    public function createAgreement($request)
    {
        if ($request->hasFile('photo'))
        {
          $image = $request->file('photo');
          $pathPhoto = Hellper::image($image, 'public/images/agreement/'.$request->user()->id.'/', 'public', 'storage');
        }else{
            $pathPhoto = '';
        }

        $dataQr = "Certificate Name: ".$request['certificate_name']."\nCertificate Number: ".$request['certificate_number']."\n".
            "Proprietor name: ".$request['proprietor_name']."\nName of concept: ".$request['name_of_concept']."\n".
            "Date of Issue: ".$request['date_of_issue']."\nCountry Issued: ".$request['country_issued']."\n".
            "Certificate Description: ".$request['certificate_description']."\n";

        $path = Hellper::QrCodeGenerator($dataQr, $request->user()->id, '');

        $data = [
            'user_id' => $request->user()->id,
            'publish'=> $request['publish'],
            'category_id'=> (int)$request['category_id'],
            'type'=> $request['type'],
            'photo' => $pathPhoto,
            'certificate_name' => $request['certificate_name'],
            'certificate_number' => $request['certificate_number'],
            'proprietor_name' => $request['proprietor_name'],
            'name_of_concept' => $request['name_of_concept'],
            'date_of_issue' => Carbon::parse($request['date_of_issue']),
            'country_issued'=> $request['country_issued'],
            'certificate_description' => $request['certificate_description'],
            'qr_code' => asset($path),
            'agreement_status' => 'pending',
        ];

       $newAgreement = $this->agreementsRepository->createAgreement($data);

        return response()->apiResponse(
            'Agreement has been created successfully',
            new AgreementResource($newAgreement));
    }

    public function updateAgreement($request, $id)
    {

        if ($request->hasFile('photo'))
        {
            $image = $request->file('photo');
            $pathPhoto = Hellper::image($image, 'public/images/agreement/'.$request->user()->id.'/');
        }else{
            $pathPhoto = '';
        }

        $dataQr = "Certificate Name: ".$request['certificate_name']."\nCertificate Number: ".$request['certificate_number']."\n".
            "Proprietor name: ".$request['proprietor_name']."\nName of concept: ".$request['name_of_concept']."\n".
            "Date of Issue: ".$request['date_of_issue']."\nCountry Issued: ".$request['country_issued']."\n".
            "Certificate Description: ".$request['certificate_description']."\n";

        $path = Hellper::QrCodeGenerator($dataQr, $request->user()->id);

        $agreement = Agreement::query()->findOrFail($id);


        $data = [
            'user_id' => $request->user()->id,
            'category_id'=> (int)$request['category_id'],
            'publish'=> $request['publish'],
            'type'=> $request['type'],
            'photo' => $request->hasFile('photo') ? $pathPhoto : $agreement->photo,
            'certificate_name' => $request['certificate_name'],
            'certificate_number' => $request['certificate_number'],
            'proprietor_name' => $request['proprietor_name'],
            'name_of_concept' => $request['name_of_concept'],
            'date_of_issue' => Carbon::parse($request['date_of_issue']),
            'country_issued'=> $request['country_issued'],
            'certificate_description' => $request['certificate_description'],
            'qr_code' => asset($path),
//            'agreement_status' => $request['agreement_status'] ?? 'registered',
            'agreement_status' => isset($request['agreement_status']) ? $request['agreement_status'] : 'pending',
        ];


        $newAgreement = $this->agreementsRepository->updateAgreement($id, $data);

        return response()->apiResponse(
            'Agreement has been updated successfully',
            new AgreementResource($newAgreement));
    }

    public function notificationAgreement()
    {
        return response()->apiResponse('',$this->agreementsRepository->notificationAgreement());
    }

    public function getPublicAgreements($category_id,$count)
    {
       return $this->agreementsRepository->getPublicAgreements($category_id,$count);
    }

    public function showAgreements()
    {

        $allAgreements = [
            'sell'=> $this->agreements('sell'),
            'license' => $this->agreements('license'),
            'charge' => $this->agreements('charge'),
            'partner' => $this->agreements('partner')
        ];


        return new ProprietorCollection($allAgreements);
    }

    public function agreements($type)
    {
        return Agreement::query()->where('type', $type)->whereIn('agreement_status', ['registered', 'pending'])->where('publish', 'public')->get();
    }

    public function allAgreements($count)
    {

        $allAgreements = [
            'sell'=> $this->getProprietorAgreements('sell',$count),
            'license' => $this->getProprietorAgreements('license',$count),
            'charge' => $this->getProprietorAgreements('charge',$count),
            'partner' => $this->getProprietorAgreements('partner',$count)
        ];


        return new ProprietorCollection($allAgreements);
    }

    public function getProprietorAgreements($type,$count)
    {
        $fname = 'Agreements';
        $get = 'get';
        try {
             switch ($type) {
                 case 'sell':
                 case 'license':
                 case 'charge':
                 case 'partner':
                     $agrementsType = $get.Str::ucfirst($type).$fname;
                     break;
             }

             return $this->{$agrementsType}($count);

        } catch (\Exception $e) {
            return response()->json(['status'=> 'Fail'],Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }


    public function getSellAgreements($count)
    {
        return $this->agreementsRepository->getSellAgreements($count);
    }
    public function getLicenseAgreements($count)
    {
        return $this->agreementsRepository->getLicenseAgreements($count);
    }
    public function getChargeAgreements($count)
    {
        return $this->agreementsRepository->getChargeAgreements($count);
    }
    public function getPartnerAgreements($count)
    {
        return $this->agreementsRepository->getPartnerAgreements($count);
    }

    public function getAgreemenById($id)
    {
       return new AgreementResource($this->agreementsRepository->getAgreemenById($id));
    }

    public function detailsAgreementById($id)
    {
       return new AgreementResource($this->agreementsRepository->detailsAgreementById($id));
    }

    public function deleteAgreementById($id)
    {
        return $this->agreementsRepository->deleteAgreementById($id);
    }

}
