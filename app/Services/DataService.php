<?php
namespace App\Services;

use App\Http\Resources\MenuResouceCollection;
use App\Http\Resources\HomeResource;
use App\Repositories\DataRepository;

class DataService
{
    public $dataRepository;

    public function __construct(DataRepository $dataRepository) {
        $this->dataRepository = $dataRepository;
    }
    public function getMenuNavBar()
    {
        return new MenuResouceCollection($this->dataRepository->getMenuNavBar());
    }
    public function getHomePageData()
    {
        return new HomeResource($this->dataRepository->getHomePageData());
    }

    public function Agreements($count)
    {
       return $this->dataRepository->getAgreements($count);
    }

    public function createBanner()
    {
       return $this->dataRepository->createHomePageData();
    }
}
