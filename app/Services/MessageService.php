<?php
namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\MessageRepository;

class MessageService
{
    public $userRepository;

    public $messageRepository;

    public function __construct(UserRepository $userRepository,MessageRepository $messageRepository) {

        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
    }

    public function sendMessage($request)
    {
        if (isset($request['message']) && !is_null($request['message']))
        {
            return $this->messageRepository->send($request);
        }

    }

    public function getProposal()
    {
        return $this->messageRepository->getProposal();
    }

    public function updateProposalMassage($request)
    {
        return $this->messageRepository->updateProposalMassage($request);
    }


}
