<?php
namespace App\Services;

use App\Hellper\Hellper;
use App\Http\Resources\Proposal\GetProposalResources;
use App\Models\Agreement;
use App\Models\NewProposal;
use App\Models\Proposal;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ProposalResource;
use App\Repositories\ProposalRepository;
use Symfony\Component\HttpFoundation\Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ProposalService
{

    public $proposalRepository;

    public function __construct(ProposalRepository $proposalRepository) {
        $this->proposalRepository = $proposalRepository;
    }
    //Admin side
    public function getAdminAgreementProposals()
    {
        return GetProposalResources::collection($this->proposalRepository->getAdminAgreementProposals());
    }

    public function getSingleAgreementProposal($proposal_id)
    {
        return $this->proposalRepository->getSingleAgreementProposal($proposal_id);
    }

    public function getAgreementProposals($type)
    {
        return GetProposalResources::collection($this->proposalRepository->getAgreementProposals($type));
    }

    // user send proposals

    public function getSingleProposalById($id)
    {
        return new GetProposalResources($this->proposalRepository->getSingleProposalById($id));        # code...
    }

    public function getSendProposals()
    {
        return GetProposalResources::collection($this->proposalRepository->getSendProposals());
//        return ProposalResource::collection($this->proposalRepository->getSendProposals());
    }

    public function getUserCustomerSingleProposal($id)
    {
        return  new GetProposalResources($this->proposalRepository->getUserCustomerSingleProposal($id));
    }






    public function createProposal($request)
    {
        $auth_user = Hellper::authApi('user')->id;
        $agreements = Agreement::query()->findOrFail($request['agreement_id']);
        $proposal = NewProposal::query()->where('status', 'pending')->where('user_id', $agreements->user_id)->where('customer_id', $auth_user)->count();
        if ($agreements && $agreements->user_id == $auth_user){
            return \response()->json('You cannot send a request to yourself', 400);
        }
        if ($proposal){
            return \response()->json('You cannot send a request again. there is an existing proposal', 400);
        }

        return $this->proposalRepository->createProposal($request, $agreements);
    }

    public function updateProposal($request,$id)
    {
        return $this->proposalRepository->updateProposal($request,$id);
    }

    public function changeStatusProposal($request)
    {
        return $this->proposalRepository->changeStatusProposal($request);
    }


}
