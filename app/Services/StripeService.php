<?php
namespace App\Services;

use Laravel\Cashier\Cashier;
use App\Repositories\UserRepository;
use App\Http\Resources\BillingResource;
use App\Http\Resources\InvoiceResource;
use App\Repositories\BillingRepository;
use App\Repositories\IpdInvoiceRepository;
use App\Repositories\PlanRepository;
use App\Repositories\ProposalRepository;
use App\Repositories\TransactionRepository;
use Illuminate\Support\Str;
use Stripe\Charge;
use Carbon\Carbon;

class StripeService
{
    private $userRepository;

    private $billingRepository;

    private $planRepository;

    private $transactionRepository;

    private $ipdInvoiceRepository;

    private $stripe;

    private $proposalRepository;

    public function __construct(
        UserRepository $userRepository,
        BillingRepository $billingRepository,
        PlanRepository $planRepository,
        TransactionRepository $transactionRepository,
        IpdInvoiceRepository $ipdInvoiceRepository,
        ProposalRepository $proposalRepository)
         {
        $this->userRepository = $userRepository;
        $this->billingRepository = $billingRepository;
        $this->planRepository = $planRepository;
        $this->transactionRepository = $transactionRepository;
        $this->ipdInvoiceRepository = $ipdInvoiceRepository;
        $this->proposalRepository = $proposalRepository;
        $this->stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
    }

    public function createCustomer()
    {

        $user = Cashier::findBillable(auth('client-api')->user()->stripe_id);
        if (is_null($user))
        {
            auth('client-api')->user()->createAsStripeCustomer();
        }

    }

    public function paymentMethod($request)
    {
       try {

        $paymentMethod = $this->stripe->paymentMethods->create([
            'type' => 'card',
            'card' => [
                'number' => $request['number'],
                'exp_month' => $request['exp_month'],
                'exp_year' => $request['exp_year'],
                'cvc' => $request['cvc'],
            ],
            'billing_details' => [
                'name' => $request['name'],
            ],
        ]);

        return $paymentMethod;

       } catch (\Throwable $th) {
        throw $th;
       }
    }

    //payment Method

    public function createPaymentMethod($request)
    {
        try {

            $this->createCustomer();

            auth('client-api')->user()->createSetupIntent();

            $paymentMethod = $this->paymentMethod($request);

            $billing = $this->billingRepository->create([
                'pm_id' => $paymentMethod->id,
                'card_number_last4' => $paymentMethod->card->last4,
                'brand' => $paymentMethod->card->brand,
                'holder_name' => $paymentMethod->billing_details->name,
                'status' => $paymentMethod->id ? 'success' : 'fail',
                'user_id' => $request->user('client-api')->id,
                'pm_method' => 0,
                'expires' => $paymentMethod->card['exp_year'].'-'.$paymentMethod->card['exp_month'],
            ]);

            $request->user('client-api')->addPaymentMethod($paymentMethod);

           return new BillingResource($billing);

        } catch (\Exception $ex) {

            return false;
        }

    }

    public function singleCharge($request)
    {
        try {
            $this->createCustomer();

            $user = $request->user('client-api');

            $user->createSetupIntent();

            $paymentMethod = $this->paymentMethod($request);

          //  $this->setDefaultMethod($paymentMethod->id);

            $charge = $user->charge(( (int)$request['price_cost'] * 100), $paymentMethod);

            $transactionData = [
                'proposal_id' => $request['proposal_id'],
                'user_id' => $user->id,
                'pm_id' => $charge->payment_method,
                'currency' => $charge->currency,
                'amount' => ($charge->amount / 100),
                'pi_id' => $charge->id,
                'ch_id' => $charge->latest_charge,
                'status' => $charge->status,
            ];

           $transaction =  $this->transactionRepository->createChargeTransaction($transactionData);
           $agreement = $this->proposalRepository->getById($request['proposal_id'])->agreement()->first();

           $invoiceDate = [

            'invoice_number' => random_int(1000000,9999999),
            'transaction_id' => $transaction->id,
            'user_id' => $user->id,
            'status' => 1,
            'date_of_issue' => Carbon::now()->format('Y-m-d'),
            'date_of_due' => Carbon::now()->format('Y-m-d'),
            'prod_type' => $agreement->type,
            'certificate_name' => $agreement->certificate_name,
            'category_id' => $agreement->category_id,
            'qti' => 1,
            'unit_price' => ($charge->amount / 100),
            'amount' => ($charge->amount / 100),

           ];


            return $this->ipdInvoiceRepository->createIpdInvoice($invoiceDate);

        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function setDefaultMethod($pm_id)
    {
        try {

            $paymentMethod = request()->user('client-api')->findPaymentMethod($pm_id);

            if (!is_null($paymentMethod))
            {
                request()->user('client-api')->updateDefaultPaymentMethod($pm_id);

                $this->billingRepository->updateBillingDefault($pm_id);

                return true;
            }


        } catch (\Exception $ex) {

            return false;
        }

    }

    public function deletePaymentMethod($id)
    {
        try {

            $paymentMethod = request()->user('client-api')->findPaymentMethod($id);
            if (!is_null($paymentMethod))
            {
                $paymentMethod->delete();

                $this->billingRepository->where('pm_id',$id)->delete();

                return true;
            }

        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getPaymentMethods()
    {
        if (request()->user('client-api')->hasPaymentMethod())
        {

            // request()->user('client-api')->paymentMethods();
            if (request()->user('client-api')->hasDefaultPaymentMethod())
            {
                $pm_id = request()->user('client-api')->defaultPaymentMethod()->id;

                 $this->billingRepository->updateBillingDefault($pm_id);
            }
            return BillingResource::collection($this->billingRepository->getBillings());
        }

       return false;
    }

    //subscriptions
    public function createPlan($request)
    {
        try {
            $plan =$this->stripe->prices->create([
                'unit_amount' => $request['amount']*100,
                'currency' => 'usd',
                'recurring' => ['interval' => $request['billing_period'] ],
                'product_data' => [
                    'name' => $request['name'],
                    'statement_descriptor' => $request['desc']
                ],
              ]);

         return $this->planRepository->create([
                'name' => $request['name'],
                'slug'=> Str::slug($request['name'], '-'),
                'year'=> $request['billing_period'],
                'stripe_prod' => $plan->product,
                'price_id' => $plan->id,
                'price' => ($plan->unit_amount)/100,
                'description' => $request['desc'],
            ]);


        } catch (\Exception $ex) {

            return false;
        }

    }

    public function deletePlan($id)
    {
        $get_price = $this->planRepository->where('stripe_prod',$id)->first();

        $this->stripe->plans->delete(
            $get_price->price_id,
            []
          );

        $this->stripe->products->delete(
            $id,
            []
        );

        return $this->planRepository->where('stripe_prod',$id)->delete();
    }

    public function getPlans()
    {
       return $this->planRepository->get();
    }

    public function show($plan,$request)
    {
        $intent = auth('client-api')->user()->createSetupIntent();

        return $intent;
    }

    //Subscriptions

    public function createSubscription($request)
    {
        $plan = $this->planRepository->getById($request->plan);

        auth('client-api')->user()->createSetupIntent();

        $subscription = $request->user('client-api')->newSubscription($request->plan, $plan->price_id)->add();

    }

    public function getSubscription()
    {
        return auth('client-api')->user()->subscriptions()->get();
    }

    public function statusSubscription($request)
    {

        if (auth('client-api')->user()->subscribed($request['id']) && !auth('client-api')->user()->subscription($request['id'])->cancelled())
        {
            return auth('client-api')->user()->subscription($request['id'])->cancel();

        }else{

           return auth('client-api')->user()->subscription($request['id'])->resume();
        }

        return false;

    }

    public function invoices()
    {
       return InvoiceResource::collection(auth('client-api')->user()->invoices());
    }

    public function downloadInvoice($invoice_id)
    {
        return request()->user('client-api')->downloadInvoice($invoice_id, [
            'vendor' => 'Your Company',
            'product' => 'Your Product',
        ], 'my-invoice');
    }

}
