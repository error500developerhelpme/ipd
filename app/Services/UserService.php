<?php
namespace App\Services;

use App\Http\Resources\InvoiceIpdResource;
use App\Http\Resources\InvoiceResource;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\UserRepository;
use App\Repositories\IpdInvoiceRepository;
use Barryvdh\DomPDF\Facade\Pdf;

class UserService
{
    public $userRepository;

    public $ipdInvoiceRepository;

    public function __construct(UserRepository $userRepository,IpdInvoiceRepository $ipdInvoiceRepository) {

        $this->userRepository = $userRepository;
        $this->ipdInvoiceRepository = $ipdInvoiceRepository;
    }

    /*public function createCustomer($request)
    {
        return $this->userRepository->createCustomer($request);
    }*/
    public function getUsers($userRole)
    {
       return $this->userRepository->getUsers($userRole);
    }

    public function getUser($id)
    {
       return $this->userRepository->getUser($id);
    }

    public function updateProfile($request)
    {
       $id = auth('client-api')->user()->id;

       return $this->userRepository->updateProfile($request,$id);
    }

    public function getUserIpdInvoices()
    {
        return $this->ipdInvoiceRepository
                    ->getUserIpdInvoices(request()->user('client-api')->id);
    }

    public function downloadIpdInvoice($invoice_id)
    {
        $invoice = $this->ipdInvoiceRepository->downloadIpdInvoice($invoice_id);
        $pdf = Pdf::loadView('invoice', ['data' => $invoice]);

        return $pdf->download('invoice.pdf');
    }

   /* public function updateCustomer($request,$id)
    {
        return $this->userRepository->updateCustomer($request,$id);
    }*/

    public function deleteUser($id)
    {
        return $this->userRepository->deleteUser($id);
    }

}
