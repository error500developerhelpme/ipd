<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AgreementFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
           'user_id'=> 1,
           'publish'=> 'public',
           'category_id'=> $this->faker->numberBetween(1, 8),
           'type'=> $this->faker->randomElement(['sell','license','charge','partner']),
           'photo'=> $this->faker->imageUrl(),
           'certificate_name'=> 'certifcate'.$this->faker->name(),
           'certificate_number'=> $this->faker->numberBetween(1, 100),
           'proprietor_name'=> $this->faker->name(),
           'name_of_concept'=> $this->faker->word(),
           'date_of_issue'=> $this->faker->dateTimeThisYear(),
           'country_issued'=> $this->faker->country(),
           'certificate_description'=> $this->faker->text(50),
           'qr_code'=> $this->faker->url(),

        ];
    }
}
