<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ConceptFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'valuer_name' => $this->faker->word(),
            'agreement_id' => $this->faker->numberBetween(0, 400),
            'value_ascertained' => $this->faker->numberBetween(0, 1),
            'currency' => $this->faker->randomElement(['usd','kny','eu']),
            'value' => $this->faker->randomDigit(),
            'comment' => $this->faker->word(),
            'image' => $this->faker->url(),

        ];
    }
}
