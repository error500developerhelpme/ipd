<?php

namespace Database\Factories;

use App\Models\Insurance;
use Illuminate\Database\Eloquent\Factories\Factory;

class InsuranceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Insurance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->company(),
            'agreement_id' => $this->faker->numberBetween(0, 400),
            'type_of_policy' => $this->faker->word(),
            'value_of_policy' => $this->faker->randomDigit(),
            'policy_date' => $this->faker->date('Y_m_d'),
            'expiry_date' => $this->faker->date('Y_m_d'),
            'policy_image' => $this->faker->url(),
        ];
    }
}
