<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Invoice;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random(),
            'date_of_issue' => $this->faker->date('Y-m-d'),
            'date_of_due' => $this->faker->date('Y-m-d'),
            'category' => Category::all()->random(),
            'desc' => $this->faker->text(),
            'qti' => 1,
            'unit_price' => $this->faker->numberBetween(1000,9000),
            'amount' => $this->faker->numberBetween(1000,9000),
        ];
    }
}
