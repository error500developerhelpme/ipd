<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            [
                'name' => 'Basic',
                'slug' => 'basic',
                'stripe_plan' => 'price_1MPNQpK6TPTgACi1zlU2uONs',
                'price' => 10,
                'description' => 'Basic'
            ],
            [
                'name' => 'Premium',
                'slug' => 'premium',
                'stripe_plan' => 'price_1MPNRCK6TPTgACi1iJFqw6Mp',
                'price' => 20,
                'description' => 'Premium'
            ]
        ];
    }
}
