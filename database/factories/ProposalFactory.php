<?php

namespace Database\Factories;

use App\Models\Agreement;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProposalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'agreement_id'=> $this->faker->numberBetween(1,50),
            'status'=> $this->faker->randomElement([NULL,'accepted','rejected']),
            'acceptance' => $this->faker->word(),
            'breach_of_terms' => $this->faker->word(),
            'capacity' => $this->faker->word(),
            'confidentiality' => $this->faker->word(),
            'consideration' => $this->faker->word(),
            'due_diligence' => $this->faker->word(),
            'duration' => $this->faker->word(),
            'intention' => $this->faker->word(),
            'indemnity' => $this->faker->word(),
            'injury' => $this->faker->word(),
            'negotiation' => $this->faker->word(),
            'non_compete' => $this->faker->word(),
            'notices' => $this->faker->word(),
            'offer' => $this->faker->name(),
            'price_cost' => $this->faker->numberBetween(1000, 10000),
            'terms_conditions' => $this->faker->word(),
            'termination' => $this->faker->word(),
            'verify_certificate' => $this->faker->randomElement([0,1]),
            'value' => $this->faker->randomNumber(5, true),
            'insure' => $this->faker->word(),
        ];
    }
}
