<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class VerificationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'agreement_id' => $this->faker->numberBetween(0, 400),
            'autority_name' => $this->faker->word(),
            'certificate_authentic' => $this->faker->numberBetween(0, 1),
            'discrepancy' => $this->faker->word(),
            'image' => $this->faker->url(),
        ];
    }
}
