<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use function PHPSTORM_META\type;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->enum('publish',['public','private'])->default('private');
            $table->integer('category_id')->nullable()->default(null);
            $table->integer('insurance_id')->nullable()->default(null);
            $table->integer('concept_id')->nullable()->default(null);
            $table->enum('type',['sell','license','charge','partner'])->nullable()->default(null);
            $table->enum('agreement_status',['pending','processing','registered', 'rejected'])->default('pending');
            $table->string('photo',255)->nullable()->default(null);
            $table->string('certificate_name',200)->nullable();
            $table->integer('certificate_number')->nullable();
            $table->string('proprietor_name')->nullable();
            $table->string('name_of_concept')->nullable();
            $table->date('date_of_issue')->nullable();
            $table->string('country_issued')->nullable();
            $table->string('certificate_description',100);
            $table->string('qr_code')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
