<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('agreement_id');
            $table->integer('customer_id');
            $table->integer('user_id')->nullable()->default(null);
            $table->enum('status',['accepted','rejected', 'registered', 'pending'])->nullable()->default(null);
            $table->string('acceptance');
            $table->string('breach_of_terms');
            $table->string('capacity');
            $table->string('confidentiality');
            $table->string('consideration');
            $table->string('due_diligence');
            $table->string('duration');
            $table->string('intention');
            $table->string('indemnity');
            $table->string('injury');
            $table->string('negotiation');
            $table->string('non_compete');
            $table->string('notices');
            $table->string('offer');
            $table->integer('price_cost');
            $table->string('terms_conditions');
            $table->string('termination');
            $table->enum('valuation_type',['value','insure','verify_certificate'])->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
