<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_number');
            $table->integer('transaction_id');
            $table->integer('user_id');
            $table->tinyInteger('status');
            $table->date('date_of_issue');
            $table->date('date_of_due');
            $table->integer('category_id');
            $table->string('prod_type')->nullable()->default(null);
            $table->string('certificate_name')->nullable()->default(null);
            $table->integer('qti');
            $table->integer('unit_price');
            $table->integer('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
