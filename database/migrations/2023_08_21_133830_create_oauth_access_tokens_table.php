<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('oauth_access_tokens')){
            Schema::create('oauth_access_tokens', function (Blueprint $table) {
                $table->string('id');
                $table->bigInteger('user_id');
                $table->bigInteger('client_id');
                $table->string('name');
                $table->text('scopes');
                $table->tinyInteger('revoked');
                $table->timestamp('expires_at');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_access_tokens');
    }
}
