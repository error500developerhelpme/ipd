<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgreementProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_proposals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('proposal_id')->constrained('new_proposals');
            $table->longText('agreement')->comment('json');
            $table->string('file')->nullable();
            $table->enum('status', ['accepted','rejected', 'registered', 'pending']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_proposals');
    }
}
