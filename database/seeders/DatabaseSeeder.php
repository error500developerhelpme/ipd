<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Agreement;
use App\Models\Category;
use App\Models\Concept;
use App\Models\Insurance;
use App\Models\Plan;
use App\Models\Proposal;
use App\Models\Verification;
use App\Models\Invoice;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
    }
}
