<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\RoleAdmin;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
          'Super_Admin',
          'Admin',
          'Moderator',
          'Verification',
          'Valuation',
          'Insurance',
          'User',
        ];

        foreach ($roles as $role){
            $rl = Role::query()->create([
                'name' => $role
            ]);
        }

        //Admin
        RoleAdmin::query()->create([
            'admin_id' => 1,
            'role_id' => 1,
        ]);

        //User
        RoleUser::query()->create([
            'user_id' => 1,
            'role_id' => 7,
        ]);

        RoleUser::query()->create([
            'user_id' => 2,
            'role_id' => 4,
        ]);

        RoleUser::query()->create([
            'user_id' => 3,
            'role_id' => 5,
        ]);

        RoleUser::query()->create([
            'user_id' => 4,
            'role_id' => 6,
        ]);







    }
}
