<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::query()->create([
            'name' => 'Super_admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'image' => 'https://dev.project.loc/1',
        ]);

        $user = User::query()->create([
            'name' => 'user',
            'email_verified_at' => '2022-12-28 11:05:49',
            'remember_token' => 'dsUY2sfqa0',
            'email' => 'user@gmail.com',
            'password' => Hash::make('password'),
            'business_name' => 'business name test',
            'phone_number' => '(541) 382-5518 x92569132',
            'status' => 'active',
        ]);

        $user = User::query()->create([
            'name' => 'Verification',
            'email_verified_at' => '2022-12-28 11:05:49',
            'remember_token' => 'dsUY2sfqa0',
            'email' => 'verification@gmail.com',
            'password' => Hash::make('password'),
            'business_name' => '',
            'phone_number' => '',
            'status' => 'active',
        ]);

        $user = User::query()->create([
            'name' => 'Valuation',
            'email_verified_at' => '2022-12-28 11:05:49',
            'remember_token' => 'dsUY2sfqa0',
            'email' => 'valuation@gmail.com',
            'password' => Hash::make('password'),
            'business_name' => '',
            'phone_number' => '',
            'status' => 'active',
        ]);

        $user = User::query()->create([
            'name' => 'Insurance',
            'email_verified_at' => '2022-12-28 11:05:49',
            'remember_token' => 'dsUY2sfqa0',
            'email' => 'insurance@gmail.com',
            'password' => Hash::make('password'),
            'business_name' => 'business name test',
            'phone_number' => '(541) 382-5518 x92569132',
            'status' => 'active',
        ]);
    }
}
