@component('mail::message')
# Reset Access!

Your reset code!
<br>
Code: {{$code}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
