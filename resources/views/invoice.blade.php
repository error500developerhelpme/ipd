<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>INVOICE</title>
    <style>
        body{
            margin: 0;
            padding: 0;
            border: 0;
            font-family: 'Roboto';
            font-style: normal;
            box-sizing: border-box;
        }
        .main-header{

            color: red;

        }
        .invoice{
                width: 123px;
                height: 38px;
                font-weight: 500;
                font-size: 32px;
                line-height: 38px;
                margin-left: 20px;
                float: left;
                color: #3C3C3C;
        }

        .logo{
            width: 123px;
            margin-left: 600px;
            font-weight: 500;
            font-size: 32px;
            color: #3C3C3C;
        }

        .header-table{
            text-align:left;
            width: 700px;
            margin: 200px 15px 50px;
            font-size: 16px;
        }

        table th{
            color: #332486;
            text-align:left;

        }

        .table-padd{
            width: 100px;
        }

        .content-table{
            text-align:left;
            width: 700px;
            margin: 100px 15px 30px;
            font-size: 20px;
            border-collapse: collapse;
            border-spacing: 0;
        }

        .content-table thead tr {
            border-bottom: 2px solid #332486;
        }

        .content-table thead tr th {
            padding-top: 20px;
        }

        .content-table tbody tr:first-child td {
            padding-top: 25px;
        }
        .content-table tbody tr{
            border-bottom: 1px solid #CAC8C8;
        }
        .content-table tbody tr td p{
           display: block;
           margin: 5px 0;
        }

        .total{
            width: 350px;
            margin-left: 395px;
            padding: 10px;
            color:#332486;
            font-size: 20px;
            font-weight: 600;
        }
        span:nth-child(2){
            padding-left: 135px;
        }

        .footer{
            font-size: 14px;
            margin-left: 15px;
        }

        .footer p:first-child {
            color: #332486;
        }

    </style>
</head>
<body>

    <div class="main-header">
        <div class="invoice">
                INVOICE
        </div>
        <div class="logo">
            <img src="logo.png" alt="logo">
        </div>
    </div>


    <table class="header-table">
        <thead>
            <tr>
                <th>Billed to</th>
                <th class="table-padd"></th>
                <th>Date of issue</th>
                <th>Invoice Number</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $data['user']->name }}</td>
                <td class="table-padd"></td>
                <td>{{ $data['date_of_issue'] }}</td>
                <td>N - {{ $data['invoice_number'] }}</td>
                <td>{{ $data['category']->name }}</td>
            </tr>
            <tr>
                <td>{{ $data['user']->email }}</td>
                <td class="table-padd"></td>
                <th style="padding-top: 20px">Date of due </th>
            </tr>

            <tr>
                <td></td>
                <td class="table-padd"></td>
                <td>{{ $data['date_of_due'] }}</td>
            </tr>
        </tbody>
    </table>

    <table class="content-table">
        <thead>
            <tr>
                <th>DESCRIPTION</th>
                <th></th>
                <th>Qty</th>
                <th>Unit Price</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <strong>{{ $data['prod_type'] }}</strong>
                    <p>{{ $data['certificate_name'] }}</p>
                </td>
                <td></td>
                <td>{{ $data['qti'] }}</td>
                <td>$ {{ $data['unit_price'] }}</td>
                <td>$ {{ $data['amount'] }}</td>
            </tr>

        </tbody>
    </table>

    <div class="total">
        <span>Total</span>
        <span>$ {{ $data['amount'] }}</span>
    </div>
    <div class="footer">
        <p>Notes</p>
        <p>Thank you for your bussines!</p>
    </div>
</body>
</html>
