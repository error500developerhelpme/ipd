<?php

use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\InsuranceController;
use App\Http\Controllers\Api\Admin\Auth\LoginController;
use App\Http\Controllers\Api\Admin\VerificationController;
use App\Http\Controllers\Api\Admin\Auth\RegisterController;
use App\Http\Controllers\Api\Admin\ValuationConceptController;
use App\Http\Controllers\Api\Admin\Auth\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('admin')->group(function (){

    Route::post('login',[LoginController::class,'login']);
    Route::post('logout',[LoginController::class,'logout']);
    Route::post('forgot-password', [ForgotPasswordController::class, 'forgotPassword']);

    Route::get('reset-password', [ForgotPasswordController::class, 'resetComplete']);
    Route::post('reset-password', [ForgotPasswordController::class, 'reset']);

    Route::middleware(['auth:admin-api','scopes:admin'])->group( function(){
        Route::post('register',[RegisterController::class,'register']);
        Route::resource('admin-accounts',AdminController::class);
        //profile
        Route::get('show-profile',[AdminController::class,'showProfile']);
        Route::put('update-profile',[AdminController::class,'updateProfile']);
        Route::put('update-password',[AdminController::class,'updatePassword']);
        // authenticated staff routes here
        Route::get('agreements',[AdminController::class, 'adminGetAgreements']);
        Route::get('agreement/{id}',[AdminController::class, 'detailsAgreementById']);
        Route::delete('delete-agreement/{id}',[AdminController::class, 'deleteAgreementById']);
        //Customers
        Route::get('get-customers',[AdminController::class, 'getCustomers']);
        Route::get('get-customer/{id}',[AdminController::class, 'getCustomer']);
        Route::delete('delete-customer/{id}',[AdminController::class, 'deleteCustomer']);
        //Proprietor
         Route::get('get-proprietors',[AdminController::class, 'getProprietors']);
         Route::get('get-proprietor/{id}',[AdminController::class, 'getProprietor']);
         Route::delete('delete-proprietor/{id}',[AdminController::class, 'deleteProprietor']);
        //Proposals
        Route::get('get-proposals',[AdminController::class, 'getProposals'])->name('get-proposals');
        Route::get('get-proposal/{id}',[AdminController::class, 'getSingleAgreementProposal']);

        Route::get('show-verify-list/{type}',[AdminController::class,'verifyAdminAgreements'])->name('show-verify-list');
        //Insurance
        Route::apiResource('insurances',InsuranceController::class);
        //Valuations
        Route::apiResource('valuations',ValuationConceptController::class);
        //Verifications
        Route::apiResource('verifications',VerificationController::class);
        //Plans
        Route::post('create-plan',[AdminController::class,'createPlan']);
        Route::get('plans', [AdminController::class, 'getPlans']);
      //  Route::put('update-plan/{id}',[AdminController::class,'updatePlan']);
        Route::delete('delete-plan/{id}',[AdminController::class,'deletePlan']);
    });

});



