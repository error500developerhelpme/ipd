<?php

use App\Http\Controllers\Api\V1\AgreementProposalController;
use App\Http\Controllers\Api\V1\MessageController;
use App\Http\Controllers\Api\V1\RequestController;
use App\Http\Controllers\Api\V1\StripeController;
use App\Http\Controllers\Api\V1\VerificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\DataController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\V1\ProprietorController;
use App\Http\Controllers\Api\Auth\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
    Route::get('/',[DataController::class,'index']);
    Route::get('/agreements/{category?}',[DataController::class,'agreements']);
    Route::get('/categories',[DataController::class,'getCategories']);
    Route::get('/details-agreement/{id}',[DataController::class,'detailsAgreementById']);
   // Route::get('banner-create',[DataController::class,'bannerCreate']);
    Route::get('/home',[DataController::class,'getHomeData']);
    Route::post('/search',[DataController::class,'search']);
});


//Route::get('/client/all-agreements',[ProprietorController::class, 'allAgreements']);


Route::prefix('client')->group(function (){

    Route::post('register',[RegisterController::class,'register']);
    Route::post('login',[LoginController::class,'login']);
    Route::post('logout',[LoginController::class,'logout']);
    Route::post('forgot-password', [ForgotPasswordController::class, 'forgotPassword']);

    Route::get('reset-password', [ForgotPasswordController::class, 'resetComplete']);
    Route::post('reset-password', [ForgotPasswordController::class, 'reset']);

    Route::get('show/agreements',[ProprietorController::class, 'showAgreements']);
    Route::get('all-agreements',[ProprietorController::class, 'allAgreements']);

    Route::get('/single-agreement/{id}',[ProprietorController::class, 'getAgreemenById']);

    Route::middleware(['auth:client-api','scopes:client'])->group( function(){
        // authenticated staff routes here

            Route::post('settings/reset-password', [ForgotPasswordController::class, 'resetPasswordSettings']);

            Route::get('show-profile',[ProprietorController::class,'getUserProfile']);
            Route::post('update-profile',[ProprietorController::class,'updateProfile']);

            Route::post('create-agreement',[ProprietorController::class, 'createAgreement']);
            Route::post('update-agreement/{id}',[ProprietorController::class, 'updateAgreement']);
            Route::get('notify-agreement',[ProprietorController::class, 'notificationAgreement']);
//            Route::get('all-agreements',[ProprietorController::class, 'allAgreements']);
            Route::get('dashboard-agreements/{type}',[ProprietorController::class, 'proprietorAgreements']);
//            Route::get('single-agreement/{id}',[ProprietorController::class, 'getAgreemenById']);
            //user incoming proposals
            Route::get('agreement-proposals/{type}',[ProprietorController::class, 'getAgreementProposals'])->name('agreement-proposals');

            //user send Proposals
            Route::post('create-proposal',[ProprietorController::class, 'createProposal']);
            Route::post('update-proposal/{id}',[ProprietorController::class, 'updateProposal']);
            Route::get('single-proposal/{id}',[ProprietorController::class, 'getSingleProposalById']);
            Route::put('change/status/proposal',[ProprietorController::class, 'changeStatusProposal']);
            Route::get('delete-proposal/{id}',[ProprietorController::class, 'deleteProposal']);

            Route::group([ 'prefix' => '/agreement/proposals'], function (){ // api/client/agreement/proposals
                Route::resource('/resource',AgreementProposalController::class);
            });

            Route::get('my-proposals',[ProprietorController::class, 'getSendProposals']);
            Route::get('my-proposal/{id}',[ProprietorController::class, 'getUserCustomerSingleProposal']);
            //proposal message
            Route::post('send-message',[ProprietorController::class, 'sendMessage']); // todo
            Route::get('notify-messages',[ProprietorController::class, 'getProposal']); // todo
            Route::put('update-messages',[ProprietorController::class, 'updateProposalMassage']); // todo
            //paymentMethods
            Route::post('create-pm-method', [ProprietorController::class, 'createPaymentMethod'])->name("create.pm.method");
            Route::get('get-cards',[ProprietorController::class,'getPaymentMethods']);
            Route::get('delete-pm-method/{id}',[ProprietorController::class,'deletePaymentMethod'])->name('delete.pm.method');
            Route::post('card/{id}',[ProprietorController::class,'setDefaultMethod']);
            //single Charge
            Route::post('charge', [ProprietorController::class, 'singleCharge']);
            //Stripe
            Route::group([ 'prefix' => '/stripe'], function (){ // api/client/stripe
                Route::post('/create/card', [StripeController::class, 'CreateCard']);
            });
            Route::get('plans', [ProprietorController::class, 'planIndex']);
          //  Route::get('plans/{plan}', [ProprietorController::class, 'planShow'])->name("plans.show");
            Route::post('create-subscription', [ProprietorController::class, 'createSubscription'])->name("subscription.create");
            Route::get('subscriptions', [ProprietorController::class, 'getSubscription']);
            Route::post('status-subscription',[ProprietorController::class, 'statusSubscription']);
            Route::get('invoices',[ProprietorController::class, 'invoices']);
            Route::get('invoice/{id}',[ProprietorController::class, 'downloadInvoice']);
          // ipd invoice
            Route::get('invoices/ipd',[ProprietorController::class, 'ipdInvoices']);
            Route::get('invoice/ipd/{id}',[ProprietorController::class, 'downloadIpdInvoice']);

        Route::group([ 'prefix' => '/request'], function (){ // api/client/request
            Route::get('/get/{type}',[RequestController::class, 'getAgreementRequest']);
        });

        Route::group([ 'prefix' => '/verify', 'middleware'=> ['verify']], function (){ // api/client/verify
            Route::get('/reject/ip/{verification}',[VerificationController::class, 'verifyRejectAgreement']);
            Route::apiResource('verifications',VerificationController::class);
        });

        Route::group([ 'prefix' => '/message'], function (){ // api/client/message
            // message
            Route::get('/messages/{room_id}', [MessageController::class, 'getMessages']);
            Route::post('/chat/send-message', [MessageController::class,'sendMessage']);

            //room
            Route::post('/create/room', [MessageController::class,'createRoom']);
            Route::get('/get/rooms', [MessageController::class, 'getRooms']);
            Route::get('/delete/{room_id}', [MessageController::class, 'deleteRoom']);

            Route::get('/search/{search}', [MessageController::class, 'getUser']);
        });

    });

});



